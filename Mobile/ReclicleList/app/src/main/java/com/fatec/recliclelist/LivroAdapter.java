package com.fatec.recliclelist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by lu-ta on 14/09/2018.
 */

public class LivroAdapter extends RecyclerView.Adapter {
     private final List<Livro> list;
     private Context ctx;


    public LivroAdapter(List<Livro> list, Context ctx) {
        this.list = list;
        this.ctx = ctx;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx)
                .inflate(R.layout.item_lista, parent, false);

        return new LivroViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        LivroViewHolder vh = (LivroViewHolder)holder;
        Livro livro = list.get(position);

        int img = ctx.getResources()
                     .getIdentifier(
                    "drawable/capa" + livro.getId(),
                    null,
                    ctx.getPackageName());

        vh.img_capa.setImageResource(img);
        vh.txt_titulo.setText(livro.getTitulo());
        vh.txt_autor.setText(livro.getAutor());
        vh.txt_editora.setText(
                String.format("%s {$d}",livro.getEditora(), livro.getAno())
        );
    }

    @Override
    public int getItemCount() {
        return (list != null ? list.size() : 0);
    }
}
