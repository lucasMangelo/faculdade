package com.fatec.recliclelist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class tela_principal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        RecyclerView rv = (RecyclerView)findViewById(R.id.rvc_livros);

        rv.setAdapter(new LivroAdapter(Livro.listar(),this));


        RecyclerView.LayoutManager m = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,
                false);
        rv.setLayoutManager(m);

    }
}
