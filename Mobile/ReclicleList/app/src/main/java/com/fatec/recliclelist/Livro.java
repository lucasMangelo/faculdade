package com.fatec.recliclelist;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lu-ta on 14/09/2018.
 */

public class Livro {

    private int id;
    private String titulo;
    private String autor;
    private String editora;
    private int ano ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public Livro(int id, String titulo, String autor, String editora, int ano) {
        this.id = id;
        this.titulo = titulo;
        this.autor = autor;
        this.editora = editora;
        this.ano = ano;
    }

    public Livro() {
    }

    public static List<Livro> listar(){
        List<Livro> livros = new ArrayList<>();

        livros.add(new Livro(1,
                "Java How to Program",
                "Paul Deitel; Harvey Deitel",
                "Pearson",2014)
        );
        livros.add(new Livro(2,
                "Computer Graphics",
                "James D. Foley, et al",
                "Pearson Education",1996)
        );
        livros.add(new Livro(3,
                "C How to Program",
                "Paul Deitel; Harvey Deitel",
                "Pearson",2010)
        );
        livros.add(new Livro(4,
                "Learning Java",
                "Patrick Niemeyer; Daniel Leuck",
                "O'Reilly",2005)
        );

        livros.add(new Livro(1,
                "Java How to Program",
                "Paul Deitel; Harvey Deitel",
                "Pearson",2014)
        );
        livros.add(new Livro(2,
                "Computer Graphics",
                "James D. Foley, et al",
                "Pearson Education",1996)
        );
        livros.add(new Livro(3,
                "C How to Program",
                "Paul Deitel; Harvey Deitel",
                "Pearson",2010)
        );
        livros.add(new Livro(4,
                "Learning Java",
                "Patrick Niemeyer; Daniel Leuck",
                "O'Reilly",2005)
        );

        return livros;
    }
}
