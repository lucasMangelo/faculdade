package com.fatec.recliclelist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by lu-ta on 14/09/2018.
 */

public class LivroViewHolder  extends RecyclerView.ViewHolder {

    final ImageView img_capa;
    final TextView txt_titulo;
    final TextView txt_autor;
    final TextView txt_editora;

    public LivroViewHolder(View view){
        super(view);

        img_capa = (ImageView)view.findViewById(R.id.img_capa);
        txt_titulo = (TextView)view.findViewById(R.id.txt_titulo);
        txt_autor = (TextView)view.findViewById(R.id.txt_autor);
        txt_editora = (TextView)view.findViewById(R.id.txt_editora);

    }
}
