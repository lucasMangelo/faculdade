package br.android.app05conectividade.controller;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.android.app05conectividade.R;
import br.android.app05conectividade.TelaGoogleMaps;
import br.android.app05conectividade.model.Result;
import br.android.app05conectividade.model.Resultado;

public class ResultadoAdapter extends RecyclerView.Adapter {

    private List<Result> resultado;
    private Context contexto;

    public ResultadoAdapter(List<Result>resultado, Context contexto){
        this.resultado = resultado;
        this.contexto = contexto;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(contexto)
                .inflate(R.layout.item,
                        viewGroup,false);

        return new ResultadoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        ResultadoViewHolder vh = (ResultadoViewHolder)viewHolder;
        Result res = resultado.get(i);

        Picasso.with(contexto)
                .load(res.getPicture().getMedium())
                .centerCrop()
                .fit()
                .into(vh.foto);

        vh.nome.setText(res.getName().getFirst().toUpperCase() + " " + res.getName().getLast().toUpperCase());
        vh.endereco.setText(res.getLocation().getStreet());
        vh.cidade.setText(res.getLocation().getCity());

    }

    @Override
    public int getItemCount() {
        return (resultado!= null)? resultado.size():0;
    }

    class ResultadoViewHolder extends RecyclerView.ViewHolder{

        final ImageView foto;
        final TextView nome;
        final TextView endereco;
        final TextView cidade;

        public ResultadoViewHolder(@NonNull View itemView) {
            super(itemView);
            foto = (ImageView)itemView.findViewById(R.id.foto);
            nome = (TextView)itemView.findViewById(R.id.nome);
            endereco = (TextView)itemView.findViewById(R.id.endereco);
            cidade = (TextView)itemView.findViewById(R.id.cidade);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int posicao = getAdapterPosition();

                    Result res = resultado.get(getAdapterPosition());
                    String latitude = res.getLocation().getCoordinates().getLatitude();
                    String longitude = res.getLocation().getCoordinates().getLongitude();

                    Intent it = new Intent(contexto,TelaGoogleMaps.class);
                    it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    it.putExtra("latitude", latitude);
                    it.putExtra("longitude", longitude);

                    contexto.startActivity(it);
                }
            });
        }


    }

}
