package br.android.app05conectividade.controller;

import br.android.app05conectividade.model.Resultado;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    @GET("?results=10&nat=br&inc=gender,name,location,picture&noinfo")
    Call<Resultado> getUsuarios();

}
