package br.android.app05conectividade;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.List;

import br.android.app05conectividade.controller.ApiService;
import br.android.app05conectividade.controller.ResultadoAdapter;
import br.android.app05conectividade.model.Result;
import br.android.app05conectividade.model.Resultado;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TelaPrincipal extends AppCompatActivity {

    private static final String TAG = TelaPrincipal.class.getSimpleName();
    public static final String BASE_URL = "https://randomuser.me/api/";
    private static Retrofit retrofit = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);
        getDadosApi();
    }

    public void getDadosApi(){
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        ApiService api = retrofit.create(ApiService.class);

        Call<Resultado> call = api.getUsuarios();
        call.enqueue(new Callback<Resultado>() {
            @Override
            public void onResponse(Call<Resultado> call, Response<Resultado> response) {
                List<Result> res = response.body().getResults();
                RecyclerView rv = (RecyclerView)findViewById(R.id.resultados);
                rv.setAdapter(new ResultadoAdapter(res,getApplicationContext()));

                RecyclerView.LayoutManager m = new LinearLayoutManager(getApplicationContext(),
                        LinearLayoutManager.VERTICAL, false);
                rv.setLayoutManager(m);
            }

            @Override
            public void onFailure(Call<Resultado> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }
}
