package br.android.app04controleprodutos.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.List;

import br.android.app04controleprodutos.R;
import br.android.app04controleprodutos.controller.CategoriaDAO;
import br.android.app04controleprodutos.controller.DatabaseHelper;
import br.android.app04controleprodutos.model.Categoria;
import br.android.app04controleprodutos.util.Util;

public class TelaPesquisa extends AppCompatActivity {

    private TableLayout tb;
    private Intent it;

    private DatabaseHelper dh;
    private CategoriaDAO dao;
    private List<Object> lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.telaFullScreen(this);
        setContentView(R.layout.activity_tela_pesquisa);

        tb = (TableLayout)findViewById(R.id.tabPesquisa);

        dh = new DatabaseHelper(this);
        dao = new CategoriaDAO();

        it = getIntent();
        if ( it != null){

            if(it.getStringExtra("campo").equals("id")){
                int chave = it.getIntExtra("chave",0);

                //Pesquisar por ID
                lista = dao.pesquisarPorId(dh, chave);

            }else if (it.getStringExtra("campo").equals("nome")){
                String chave = it.getStringExtra("chave");
                if (chave.equals("")){
                    //Lista todos os registros
                    lista = dao.listar(dh);
                }else{
                    //Pesquisar por Nome
                    lista = dao.pesquisarPorNome(dh,chave);
                }
            }
            exibirResultado();
        }else{
            finish();
        }
    }

    private void exibirResultado(){
        if ( lista != null ){
            if (lista.size() > 0 ){

                for(Object obj: lista){

                    Categoria cat = (Categoria)obj;

                    //Adicionar uma nova linha
                    TableRow tr = new TableRow(this);

                    //Conteúdo da primeira coluna
                    TextView col1 = new TextView(this);
                    col1.setText(String.valueOf(cat.getId()));
                    tr.addView(col1);

                    //Conteúdo da segunda coluna
                    TextView col2 = new TextView(this);
                    col2.setText(cat.getNome());
                    tr.addView(col2);

                    //Incluir a nova linha na tabela
                    tb.addView(tr);
                }

            }
        }
    }
}
