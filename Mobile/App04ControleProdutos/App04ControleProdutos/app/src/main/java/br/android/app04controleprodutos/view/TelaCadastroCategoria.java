package br.android.app04controleprodutos.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import br.android.app04controleprodutos.R;
import br.android.app04controleprodutos.controller.CategoriaDAO;
import br.android.app04controleprodutos.controller.DatabaseHelper;
import br.android.app04controleprodutos.model.Categoria;
import br.android.app04controleprodutos.util.Util;


public class TelaCadastroCategoria extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private ImageButton btnNovo;
    private ImageButton btnSalvar;
    private ImageButton btnDeletar;
    private ImageButton btnCancelar;
    private EditText txtId;
    private EditText txtNome;
    private Spinner spLista;

    private DatabaseHelper dh;
    private CategoriaDAO dao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.telaFullScreen(this);
        setContentView(R.layout.activity_tela_cadastro_categoria);

        btnNovo = (ImageButton)findViewById(R.id.btnNovo);
        btnSalvar = (ImageButton)findViewById(R.id.btnSalvar);
        btnDeletar = (ImageButton)findViewById(R.id.btnDeletar);
        btnCancelar = (ImageButton)findViewById(R.id.btnCancelar);
        txtId = (EditText)findViewById(R.id.txtId);
        txtNome = (EditText)findViewById(R.id.txtNome);
        spLista = (Spinner)findViewById(R.id.spLista);

        btnNovo.setOnClickListener(this);
        btnSalvar.setOnClickListener(this);
        btnDeletar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);
        spLista.setOnItemSelectedListener(this);

        dh = new DatabaseHelper(this);
        dao = new CategoriaDAO();
        carregarCategorias();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnNovo:
                txtId.setText("");
                txtNome.setText("");
                txtNome.requestFocus();
                break;

            case R.id.btnSalvar:
                Categoria cat = new Categoria();
                if (!txtId.getText().toString().equals("")){
                    cat.setId(Integer.parseInt(txtId.getText().toString()));
                }else{
                    cat.setId(0);
                }
                cat.setNome(txtNome.getText().toString());

                long id = -1;
                if (cat.getId() == 0){
                    id = dao.inserir(dh,cat);
                    txtId.setText(String.valueOf(id));
                }else{
                    id = dao.atualizar(dh, cat);
                }

                if ( id != -1 ){
                    Util.exibirMensagem(this, "Operação realizada com sucesso!");
                }else{
                    Util.exibirMensagem(this, "Ocorreu um erro na realização da operação.");
                }

                carregarCategorias();

                break;
            case R.id.btnDeletar:

                if (!txtId.getText().toString().equals("")){

                    final AlertDialog.Builder dlg = new AlertDialog.Builder(this);
                    dlg.setTitle("Controle de Produtos");
                    dlg.setCancelable(false);
                    dlg.setMessage("Tem certeza que deseja excluir o registro?");
                    dlg.setNegativeButton("Sim", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // ID do registro que será apagado
                            int delete_id = Integer.parseInt(txtId.getText().toString());
                            dao.deletar(dh, delete_id);

                            Toast.makeText(getApplicationContext(),"Operação realizada com sucesso!",
                                    Toast.LENGTH_LONG).show();

                            carregarCategorias();
                        }
                    });
                    dlg.setPositiveButton("Não", null);
                    dlg.show();
                }else{
                    Util.exibirMensagem(this, "Selecione o registro que desejado para remoção.");
                }
                break;
            case R.id.btnCancelar:
                this.finish();
                break;
        }
    }

    private void carregarCategorias(){

        List<Object> lista = dao.listar(dh);

        if (lista != null) {

            ArrayAdapter<Object> adpt = new ArrayAdapter<Object>(this,
                    android.R.layout.simple_spinner_item, lista);
            adpt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            Spinner spLista = (Spinner) findViewById(R.id.spLista);
            spLista.setAdapter(adpt);
        }
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId()==R.id.spLista){
            Categoria cat = (Categoria)parent.getItemAtPosition(position);
            txtId.setText(String.valueOf(cat.getId()));
            txtNome.setText(cat.getNome());
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}

