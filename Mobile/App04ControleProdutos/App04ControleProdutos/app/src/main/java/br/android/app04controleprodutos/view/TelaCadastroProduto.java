package br.android.app04controleprodutos.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import br.android.app04controleprodutos.R;
import br.android.app04controleprodutos.controller.CategoriaDAO;
import br.android.app04controleprodutos.controller.DatabaseHelper;
import br.android.app04controleprodutos.model.Categoria;
import br.android.app04controleprodutos.util.Util;

public class TelaCadastroProduto extends AppCompatActivity implements View.OnClickListener {

    private ImageButton btnNovo;
    private ImageButton btnSalvar;
    private ImageButton btnDeletar;
    private ImageButton btnCancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.telaFullScreen(this);
        setContentView(R.layout.activity_tela_cadastro_produto);

        btnNovo = (ImageButton)findViewById(R.id.btnNovo);
        btnSalvar = (ImageButton)findViewById(R.id.btnSalvar);
        btnDeletar = (ImageButton)findViewById(R.id.btnDeletar);
        btnCancelar = (ImageButton)findViewById(R.id.btnCancelar);
        btnNovo.setOnClickListener(this);
        btnSalvar.setOnClickListener(this);
        btnDeletar.setOnClickListener(this);
        btnCancelar.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnNovo:
                break;
            case R.id.btnSalvar:
                break;
            case R.id.btnDeletar:
                break;
            case R.id.btnCancelar:
                this.finish();
                break;
        }
    }
}
