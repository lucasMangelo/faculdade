package br.android.app04controleprodutos.controller;

public interface IDatabaseHelper {

    //Versão da base de dados
    public static final int DATABASE_VERSION = 1;

    //Nome da base de dados
    public static final String DATABASE_NAME="controle_produtos.db";

    //Nomes das tabelas
    public static final String TABLE_PRODUTO="produto";
    public static final String TABLE_CATEGORIA="categoria";

    //Nomes das colunas (comuns)
    public static final String COL_ID="id";
    public static final String COL_NOME="nome";

    // Tabela Produto = colunas específicas
    public static final String COL_PRECO="preco";
    public static final String COL_ID_CATEGORIA="id_categoria";

    //
    // INSTRUÇÕES para CRIAÇÃO DAS TABELAS
    //
    public static final String CREATE_TABLE_PRODUTO =
            "CREATE TABLE " + TABLE_PRODUTO + " (" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_NOME + " TEXT, " +
                    COL_PRECO + " REAL, " +
                    COL_ID_CATEGORIA + " INTEGER" + ")";

    public static final String CREATE_TABLE_CATEGORIA =
            "CREATE TABLE " + TABLE_CATEGORIA + " (" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_NOME + " TEXT " + ")";
}
