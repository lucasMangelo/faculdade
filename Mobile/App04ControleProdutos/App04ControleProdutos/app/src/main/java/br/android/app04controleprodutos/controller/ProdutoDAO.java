package br.android.app04controleprodutos.controller;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

import br.android.app04controleprodutos.model.Categoria;
import br.android.app04controleprodutos.model.Produto;

public class ProdutoDAO implements DAO, IDatabaseHelper {

    private SQLiteDatabase db;

    @Override
    public long inserir(DatabaseHelper db, Object obj) {

        Produto prod = (Produto)obj;
        this.db = db.getWritableDatabase();

        ContentValues val = new ContentValues();
        val.put("nome", prod.getNome());
        val.put("preco", prod.getPreco());
        val.put("id_categoria", prod.getIdCategoria());

        long id = this.db.insert(
            "produto",
            null,
            val
        );

        return id;
    }

    @Override
    public long atualizar(DatabaseHelper db, Object obj) {
        Produto prod = (Produto)obj;
        this.db = db.getWritableDatabase();

        ContentValues val = new ContentValues();
        val.put("nome", prod.getNome());
        val.put("preco", prod.getPreco());
        val.put("id_categoria", prod.getIdCategoria());

        //Definir o ID que será alterado
        String where = "id = ?";
        String whereArgs[] = {String.valueOf(prod.getId())};

        //Executar a OPERAÇÃO
        long id = this.db.update(
                "produto",
                val,
                where,
                whereArgs
        );

        return id;
    }

    @Override
    public void deletar(DatabaseHelper db, int id) {

    }

    @Override
    public List<Object> listar(DatabaseHelper db) {
        return null;
    }

    @Override
    public List<Object> pesquisarPorId(DatabaseHelper db, int id) {
        return null;
    }

    @Override
    public List<Object> pesquisarPorNome(DatabaseHelper db, String nome) {
        return null;
    }
}
