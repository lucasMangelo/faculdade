package br.android.app04controleprodutos.controller;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.List;

import br.android.app04controleprodutos.model.Categoria;

public class CategoriaDAO implements DAO, IDatabaseHelper {

    private SQLiteDatabase db;

    @Override
    public long inserir(DatabaseHelper db, Object obj) {

        //Transformar o OBJ em um objeto Categoria
        Categoria cat = (Categoria)obj;

        //Habilitar o modo ESCRITA
        this.db = db.getWritableDatabase();

        //Especificar os valores para INSERÇÃO
        ContentValues val = new ContentValues();
        val.put("nome", cat.getNome());

        //Executar a operação de INSERÇÃO
        long id = this.db.insert(
            "categoria",
            null,
            val
        );

        return id;


    }

    @Override
    public long atualizar(DatabaseHelper db, Object obj) {

        Categoria cat = (Categoria)obj;
        this.db = db.getWritableDatabase();

        ContentValues val = new ContentValues();
        val.put("nome", cat.getNome());

        //Definir o ID que será alterado
        String where = "id = ?";
        String whereArgs[] = {String.valueOf(cat.getId())};

        //Executar a OPERAÇÃO
        long id = this.db.update(
                "categoria",
                val,
                where,
                whereArgs
        );

        return id;
    }

    @Override
    public void deletar(DatabaseHelper db, int id) {
        this.db = db.getWritableDatabase();
        String where = IDatabaseHelper.COL_ID + "= ?";
        String whereArgs[] = new String[]{String.valueOf(id)};

        this.db.delete(
                IDatabaseHelper.TABLE_CATEGORIA,    // nome da tabela
                where,                              // where
                whereArgs                           // argumento
        );
    }

    @Override
    public List<Object> listar(DatabaseHelper db) {

        String SQL = "SELECT * FROM categoria ORDER BY id";
        String where[] = null;

        this.db = db.getReadableDatabase();

        //Executar a operação de SELEÇÃO
        Cursor c = this.db.rawQuery(SQL,where);

        if (c.moveToFirst()){
            List<Object> res = new ArrayList<>();
            do{
                Categoria cat = new Categoria();
                cat.setId(c.getInt(0));
                cat.setNome(c.getString(1));
                res.add(cat);
            }while(c.moveToNext());
            return res;
        }
        return null;
    }

    @Override
    public List<Object> pesquisarPorId(DatabaseHelper db, int id) {
        List<Object> lista = new ArrayList<Object>();
        String sql = "SELECT * FROM " + IDatabaseHelper.TABLE_CATEGORIA +
                " WHERE " + IDatabaseHelper.COL_ID + "=?";
        String whereArgs[] = new String[]{String.valueOf(id)};

        //Definir MODO de LEITURA nas TABELAS
        this.db = db.getReadableDatabase();

        //Criar CURSOR para os resultados
        Cursor c = this.db.rawQuery(sql, whereArgs);

        // Se retornou resultados
        if (c.moveToFirst()){
            //Adicionar resultados na lista
            do{
                Categoria cat = new Categoria();
                cat.setId(c.getInt(c.getColumnIndex(IDatabaseHelper.COL_ID)));
                cat.setNome(c.getString(c.getColumnIndex(IDatabaseHelper.COL_NOME)));
                lista.add(cat);
            }while(c.moveToNext());
        }

        //Fechar o cursor
        if (c != null && !c.isClosed()){
            c.close();
        }

        return lista;
    }

    @Override
    public List<Object> pesquisarPorNome(DatabaseHelper db, String nome) {
        List<Object> lista = new ArrayList<Object>();
        String sql = "SELECT * FROM " + IDatabaseHelper.TABLE_CATEGORIA +
                " WHERE " + IDatabaseHelper.COL_NOME+ " like ?";
        String whereArgs[] = new String[]{"%"+nome+"%"};

        //Definir MODO de LEITURA nas TABELAS
        this.db = db.getReadableDatabase();

        //Criar CURSOR para os resultados
        Cursor c = this.db.rawQuery(sql, whereArgs);

        // Se retornou resultados
        if (c.moveToFirst()){
            //Adicionar resultados na lista
            do{
                Categoria cat = new Categoria();
                cat.setId(c.getInt(c.getColumnIndex(IDatabaseHelper.COL_ID)));
                cat.setNome(c.getString(c.getColumnIndex(IDatabaseHelper.COL_NOME)));
                lista.add(cat);
            }while(c.moveToNext());
        }

        //Fechar o cursor
        if (c != null && !c.isClosed()){
            c.close();
        }

        return lista;
    }

    public int getTotalCategorias(DatabaseHelper db){
        String sql = "SELECT * FROM " + IDatabaseHelper.TABLE_CATEGORIA;
        String whereArgs[] = null;

        this.db = db.getReadableDatabase();
        Cursor c = this.db.rawQuery(sql, whereArgs);

        int count = c.getCount();
        c.close();
        return count;
    }

}
