package br.android.app04controleprodutos.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

import br.android.app04controleprodutos.R;
import br.android.app04controleprodutos.util.Util;

public class TelaPrincipal extends AppCompatActivity implements View.OnClickListener{

    private Button btnProduto;
    private Button btnCategoria;
    private Button btnPesquisa;
    private EditText txtPesquisa;
    private Intent it;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.telaFullScreen(this);
        setContentView(R.layout.activity_tela_principal);
        btnProduto = (Button)findViewById(R.id.btnProduto);
        btnCategoria = (Button)findViewById(R.id.btnCategoria);
        btnPesquisa = (Button)findViewById(R.id.btnPesquisa);
        txtPesquisa = (EditText)findViewById(R.id.txtPesquisa);

        btnProduto.setOnClickListener(this);
        btnCategoria.setOnClickListener(this);
        btnPesquisa.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnProduto:
                it = new Intent(getApplicationContext(),TelaCadastroProduto.class);
                startActivity(it);
                break;
            case R.id.btnCategoria:
                it = new Intent(getApplicationContext(),TelaCadastroCategoria.class);
                startActivity(it);
                break;
            case R.id.btnPesquisa:

                // Chave utilizada para pesquisa
                String chave = txtPesquisa.getText().toString();
                it = new Intent(getApplicationContext(),TelaPesquisa.class);

                RadioGroup rg = (RadioGroup)findViewById(R.id.rgTipoPesquisa);
                if (rg.getCheckedRadioButtonId() == R.id.rbPesquisaPorId){
                    try{
                        int chave_id = Integer.parseInt(chave);
                        it.putExtra("campo","id");
                        it.putExtra("chave", chave_id);
                        startActivity(it);
                    }catch(Exception e){
                        Util.exibirMensagem(this,"O valor fornecido não é válido para pesquisa por ID.");
                        txtPesquisa.requestFocus();
                    }
                }else{
                    it.putExtra("campo","nome");
                    it.putExtra("chave", chave);
                    startActivity(it);
                }
                break;
        }
    }
}
