package com.first_application.first_application;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TelaPrincipal extends AppCompatActivity implements View.OnClickListener{

    //Declarar os controles da UI que serão programados
    private EditText edtvalores1;
    private EditText edtvalores2;
    private Button btnReset;
    private Button btnSubtrair;
    private Button btnMultiplicar;
    private Button btnSomar;
    private Button btnDividir;
    private Button btnNum0;
    private Button btnNum1;
    private Button btnNum2;
    private Button btnNum3;
    private Button btnNum4;
    private Button btnNum5;
    private Button btnNum6;
    private Button btnNum7;
    private Button btnNum8;
    private Button btnNum9;
    private Button btnComma;
    private Button btnResult;
    private Button ButtonSelected;
    private TextView txtResultado;

    //valores

    private List<String> valores;

    private List<Double> numeros;

    private List<String> operadores;

    private Double resultado;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        //Referenciar os controle da UI
        btnReset = (Button)findViewById(R.id.btnReset);
        btnDividir = (Button)findViewById(R.id.btnDividir);
        btnMultiplicar = (Button)findViewById(R.id.btnMultiplicar);
        btnSomar = (Button)findViewById(R.id.btnSomar);
        btnSubtrair = (Button)findViewById(R.id.btnSubtrair);
        btnResult = (Button)findViewById(R.id.btnResult);

        // Numeros
        btnNum0 = (Button)findViewById(R.id.btnNum0);
        btnNum1 = (Button)findViewById(R.id.btnNum1);
        btnNum2 = (Button)findViewById(R.id.btnNum2);
        btnNum3 = (Button)findViewById(R.id.btnNum3);
        btnNum4 = (Button)findViewById(R.id.btnNum4);
        btnNum5 = (Button)findViewById(R.id.btnNum5);
        btnNum6 = (Button)findViewById(R.id.btnNum6);
        btnNum7 = (Button)findViewById(R.id.btnNum7);
        btnNum8 = (Button)findViewById(R.id.btnNum8);
        btnNum9 = (Button)findViewById(R.id.btnNum9);

        //Virgula
//        /btnComma = (Button)findViewById(R.id.btnComma);

        txtResultado = (TextView)findViewById(R.id.txtResultado);

        // Indicar que o evento clique será tratado pelo botão calcular

        btnReset.setOnClickListener(this);
        btnDividir.setOnClickListener(this);
        btnMultiplicar.setOnClickListener(this);
        btnSomar.setOnClickListener(this);
        btnSubtrair.setOnClickListener(this);
        btnResult.setOnClickListener(this);

        btnNum0.setOnClickListener(this);
        btnNum1.setOnClickListener(this);
        btnNum2.setOnClickListener(this);
        btnNum3.setOnClickListener(this);
        btnNum4.setOnClickListener(this);
        btnNum5.setOnClickListener(this);
        btnNum6.setOnClickListener(this);
        btnNum7.setOnClickListener(this);
        btnNum8.setOnClickListener(this);
        btnNum9.setOnClickListener(this);

        //btnComma.setOnClickListener(this);

        this.valores = new ArrayList<String>();
        this.numeros = new ArrayList<Double>();
        this.operadores = new ArrayList<String>();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSubtrair:
                this.addValor("-");
                break;
            case R.id.btnSomar:
                this.addValor("+");
                break;
            case R.id.btnDividir:
                this.addValor("/");
                break;
            case R.id.btnMultiplicar:
                this.addValor("*");
                break;
            case R.id.btnNum0:
                this.addValor("0");
                break;
            case R.id.btnNum1:
                this.addValor("1");
                break;
            case R.id.btnNum2:
                this.addValor("2");
                break;
            case R.id.btnNum3:
                this.addValor("3");
                break;
            case R.id.btnNum4:
                this.addValor("4");
                break;
            case R.id.btnNum5:
                this.addValor("5");
                break;
            case R.id.btnNum6:
                this.addValor("6");
                break;
            case R.id.btnNum7:
                this.addValor("7");
                break;
            case R.id.btnNum8:
                this.addValor("8");
                break;
            case R.id.btnNum9:
                this.addValor("9");
                break;
            case R.id.btnResult:
                this.addValor("=");
                break;
            case R.id.btnReset:
                this.valores.clear();
                this.numeros.clear();
                this.operadores.clear();
                this.ButtonSelected = null;
                break;
        }

        setResultado();
    }

    private void showDialog(String msg){
        AlertDialog.Builder dlg = new AlertDialog.Builder(this);
        dlg.setMessage(msg);
        dlg.setPositiveButton("ok", null);
        dlg.show();
    }

    private void setResultado(){

        Double v1 = null, v2 = null,result = null;
        String operador = "", msg = "";

        for(Iterator<String> iter = valores.iterator();
            iter.hasNext();) {
            msg += iter.next();
        }

        this.txtResultado.setText(msg);
    }

    private String addMessage(Double value, String msg, String operator){
        String aux = "%.2f ";

        if(!operator.isEmpty())
            aux += operator + " ";

        msg += String.format(aux, value);

        return msg;
    }

    private void addValor(String valor){

        this.numeros.clear();
        this.operadores.clear();

        int total = valores.size() - 1;

        String aux = "";

        for(String caracter : valores) {

            if(!isNumeric(caracter)){

                this.numeros.add(Double.parseDouble(aux));
                this.operadores.add(caracter);
                aux = "";
            }
            else
                aux += caracter;
        }

        if(aux != "" && isNumeric(aux))
            this.numeros.add(Double.parseDouble(aux));

        if(valor == "="){

            double resultado = this.calculaValores();

            if(!Double.isNaN(resultado)){
                this.valores.clear();
                this.operadores.clear();
                this.valores.add(String.valueOf(resultado));
            }
        }else
            this.valores.add(valor);
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public double calculaValores()
    {
        int count = 0;
        double result = 0, val = 0;

        for (double num : this.numeros) {

            val = num;

            if(count > 0){
                double aux = this.numeros.get(count - 1);
                String operador = this.operadores.get(count - 1);

                switch (operador){
                    case "-":
                        if (result == 0)
                            result = aux - val;
                        else
                            result -= val;
                        break;
                    case "+":
                        if (result == 0)
                            result = aux + val;
                        else
                            result += val;
                        break;
                    case "*":
                        if (result == 0)
                            result = aux * val;
                        else
                            result *= val;
                        break;
                    case "/":
                        if (result == 0)
                            result = aux / val;
                        else
                            result /= val;
                        break;
                }
            }
            count ++;
        }

        return result;
    }
}
