package com.pizzaria_fatec.pizzaria_app;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class TelaFinalizar extends AppCompatActivity implements View.OnClickListener {

    private Button btnFinalizar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_finalizar);

        btnFinalizar = (Button)findViewById(R.id.btn_finalizar);
        btnFinalizar.setOnClickListener(this);

        //Recuperar os dados da TelaPrincipal
        Intent it = getIntent();
        if (it != null){
            EditText edt1 = (EditText)findViewById(R.id.edit_descricao);
            EditText edt2 = (EditText)findViewById(R.id.edit_preco);

            String descricao =
                    "Sabor: " + it.getStringExtra("sabor") + "\n" +
                            "Tamanho: " + it.getStringExtra("tamanho") + "\n" +
                            "Adicionais: " + it.getStringExtra("adicionais") + "\n" +
                            "Retirar no balcão: " + it.getStringExtra("retirar");
            edt1.setText(descricao);

            edt2.setText(
                    String.format("R$ %.2f", it.getDoubleExtra("preco",0))
            );

        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_finalizar) {
            // Fecha a atividade atual
            //finish();

            criarNotificacao(
                    "Pedido",
                    "O pedido foi realizado com sucesso.\n" +
                            "Você já pode retirar a sua pizza. \n" +
                            "\n\nObrigado pela preferência."
            );

            Intent it = new Intent(
                    getApplicationContext(),
                    MainActivity.class
            );

            it.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(it);
        }
    }

    private void criarNotificacao(String titulo, String texto){

        // 01. Definir as propriedades da Notificação
        final int NOTIFICATION_ID = 123;
        final String CHANNEL_ID = "Notificação";

        // 02. Instanciar o gerenciador de notificações
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);


        // 03. Definir um Canal de Notificação para API >= 28
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel canal = new NotificationChannel(CHANNEL_ID, "canal", importance);
            canal.setDescription("Canal de Notificação");
            canal.enableLights(true);
            canal.setLightColor(Color.RED);
            canal.enableVibration(true);
            canal.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            canal.setShowBadge(true);
            notificationManager.createNotificationChannel(canal);
        }

        // 04. Especificar o ícone, o título e a mensagem da notificação
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.pizza_icon_16)
                .setContentTitle(titulo)
                .setStyle(
                        new NotificationCompat.BigTextStyle().bigText(texto)
                )
                .setContentText(texto);

        // 05. Definir qual Atividade será chamada quando o usuário clicar na notificação
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(new Intent(this, MainActivity.class));
        PendingIntent it = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(it);

        // 06. Exibir a notificação
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

}
