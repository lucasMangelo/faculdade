package com.pizzaria_fatec.pizzaria_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btnAdicionar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnAdicionar = (Button)findViewById(R.id.btn_adicionar);
        btnAdicionar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_adicionar){
            // Definir a estratégia de navegação

            Intent it = new Intent(
                    getApplicationContext(), // Tela Origem
                    TelaFinalizar.class // Tela Destino
            );

            // Retornar o sabor da pizza

            Spinner spnSabor = (Spinner)findViewById(R.id.spn_Sabor);

            int posicao = spnSabor.getSelectedItemPosition(); // Posição no ArrayList
            String valor = spnSabor.getSelectedItem().toString(); // Descrição do item selecionado


            it.putExtra("sabor",valor); // envia o dado para a tela Finalizar

            // Retornar o tamanho da pizza

            RadioGroup rdgTamanho = (RadioGroup)findViewById(R.id.rdg_tamanho);

            String tamanho = "";
            double preco = 0;

            switch (rdgTamanho.getCheckedRadioButtonId()){
                case R.id.rdb_pqueno:
                    tamanho = "pequeno";
                    preco = 30;
                    break;
                case R.id.rdb_medio:
                    tamanho = "medio";
                    preco = 45;
                    break;
                case  R.id.rdb_grande:
                    tamanho = "grande";
                    preco = 85;
                    break;
            }

            it.putExtra("tamanho",tamanho);

            // Retornar os adicionais

            CheckBox ckb_borda = (CheckBox)findViewById(R.id.ckb_borda);
            CheckBox ckb_queijo = (CheckBox)findViewById(R.id.ckb_queijo);
            CheckBox ckb_bacon = (CheckBox)findViewById(R.id.ckb_bacon);
            CheckBox ckb_dobro = (CheckBox)findViewById(R.id.ckb_dobro);
            CheckBox ckb_cebola = (CheckBox)findViewById(R.id.ckb_cebola);

            String adicionais = "";

            double precoAdicional = 0;

            if(ckb_borda.isChecked()){
                adicionais += "Borda extra;";
                precoAdicional += 0.05;
            }

            if(ckb_queijo.isChecked()){
                adicionais += "Queijo extra;";
                precoAdicional += 0.03;
            }

            if(ckb_bacon.isChecked()){
                adicionais += "Bacon extra;";
                precoAdicional += 0.05;
            }

            if(ckb_dobro.isChecked()){
                adicionais += "Recheiro em dobro;";
                precoAdicional += 0.10;
            }

            if (ckb_cebola.isChecked()){
                adicionais += "Sem Cebola;";
            }

            preco += preco * precoAdicional;

            it.putExtra("adicionais",adicionais);
            it.putExtra("precoAdicional",precoAdicional);
            it.putExtra("preco",preco);

            Switch swt = (Switch)findViewById(R.id.sw_retirar);

            if(swt.isChecked())
                it.putExtra("retirar","Sim");
            else
                it.putExtra("retirar","Não");

            // Abrir a tela Finalizar
            startActivity(it);
        }
    }
}
