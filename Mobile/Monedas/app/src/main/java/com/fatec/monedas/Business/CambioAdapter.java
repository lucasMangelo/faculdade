package com.fatec.monedas.Business;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fatec.monedas.Common.Utils;
import com.fatec.monedas.Model.ICurrency;
import com.fatec.monedas.Model.Movimentacao;
import com.fatec.monedas.MovimentacaoActivity;
import com.fatec.monedas.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CambioAdapter  extends RecyclerView.Adapter {
    private final List<ICurrency> list;
    private Context ctx;

    public CambioAdapter(List<ICurrency> list, Context ctx) {
        this.list = list;
        this.ctx = ctx;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx)
                .inflate(R.layout.cambio_lista, parent, false);

        return new CambioViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        CambioViewHolder vh = (CambioViewHolder)holder;
        ICurrency obj = list.get(position);

        vh.txt_currency.setText(obj.getName());

//        String valor = String.format("%.2f", obj.getValor().toString());
        vh.txt_buy.setText(obj.getBuy());
    }

    @Override
    public int getItemCount() {
        return (list != null ? list.size() : 0);
    }
}