package com.fatec.monedas;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.fatec.monedas.Controller.DatabaseHelper;
import com.fatec.monedas.Controller.UsuarioLogadoDAO;
import com.fatec.monedas.Model.UsuarioLogado;

import java.util.List;

public class SplashActivity extends AppCompatActivity {

    private DatabaseHelper dh;
    private UsuarioLogadoDAO dao;
    private UsuarioLogado user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        dh = new DatabaseHelper( this );
        dao = new UsuarioLogadoDAO();

        user =  dao.getUsuarioLogado(dh);
        int tempo = 2000;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent it = new Intent(SplashActivity.this, user != null ? DashBoardActivity.class : LoginActivity.class);

                startActivity(it);
                finish();
            }
        }, tempo);
    }
}