package com.fatec.monedas.Model;

public class Results {
    private String[] available_sources;

    private Currencies currencies;

    private Bitcoin bitcoin;

    private Stocks stocks;

    public String[] getAvailable_sources ()
    {
        return available_sources;
    }

    public void setAvailable_sources (String[] available_sources)
    {
        this.available_sources = available_sources;
    }

    public Currencies getCurrencies ()
    {
        return currencies;
    }

    public void setCurrencies (Currencies currencies)
    {
        this.currencies = currencies;
    }

    public Bitcoin getBitcoin ()
    {
        return bitcoin;
    }

    public void setBitcoin (Bitcoin bitcoin)
    {
        this.bitcoin = bitcoin;
    }

    public Stocks getStocks ()
    {
        return stocks;
    }

    public void setStocks (Stocks stocks)
    {
        this.stocks = stocks;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [available_sources = "+available_sources+", currencies = "+currencies+", bitcoin = "+bitcoin+", stocks = "+stocks+"]";
    }
}
