package com.fatec.monedas.Controller;

import com.fatec.monedas.Model.Conversion;

import retrofit2.Call;
import retrofit2.http.GET;

public interface FixerService {
    @GET("quotations?format=json&key=80a3a312")
    Call<Conversion> getCambio();
}
