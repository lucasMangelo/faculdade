package com.fatec.monedas.Business;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fatec.monedas.Common.Utils;
import com.fatec.monedas.Model.Movimentacao;
import com.fatec.monedas.MovimentacaoActivity;
import com.fatec.monedas.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MovimentacaoAdapter extends RecyclerView.Adapter {
    private final List<Movimentacao> list;
    private Context ctx;



    public MovimentacaoAdapter(List<Movimentacao> list, Context ctx) {
        this.list = list;
        this.ctx = ctx;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx)
                .inflate(R.layout.movimentacao_lista, parent, false);

        return new MovimentacaoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        MovimentacaoViewHolder vh = (MovimentacaoViewHolder)holder;
        Movimentacao obj = list.get(position);

        vh.txt_descricao.setText(obj.getDescricao());

//        String valor = String.format("%.2f", obj.getValor().toString());
        vh.txt_valor.setText(Utils.showDouble(obj.getValor()));

        vh.movimentacao_id.setText(Integer.toString(obj.getId()));

        Drawable backgroud = null;
        String tipo = obj.getTipo();
        if(tipo.equals("Receita"))
            backgroud = ContextCompat.getDrawable(this.ctx, R.drawable.border_green_top_bottom);
        else
            backgroud = ContextCompat.getDrawable(this.ctx, R.drawable.border_red_top_bottom);

        vh.lyt_content.setBackground(backgroud);

        vh.btn_delete.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                String descricao = ((MovimentacaoViewHolder) holder).txt_descricao.getText().toString();

                AlertDialog.Builder builder = new AlertDialog.Builder( ctx);

                builder.setTitle("Atenção");

                builder.setMessage("Deseja excluir o item " + descricao + " ?");

                builder.setCancelable(true);

                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Utils.showToast("Excluído com sucesso",ctx);
                    }
                });

                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        });

        vh.btn_edit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent it = new Intent(
                        ctx, // Tela Origem
                        MovimentacaoActivity.class // Tela Destino
                );

                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

                String descricao = ((MovimentacaoViewHolder) holder).txt_descricao.getText().toString(),
                        valor = ((MovimentacaoViewHolder) holder).txt_valor.getText().toString(),
                        data = format.format(new Date( )),
                        id = ((MovimentacaoViewHolder) holder).movimentacao_id.getText().toString();

                it.putExtra( "id", id );

                ctx.startActivity(it);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (list != null ? list.size() : 0);
    }
}
