package com.fatec.monedas.Controller;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.fatec.monedas.Model.Movimentacao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class BaseDAO<T> {

    private String tableName;

    protected SQLiteDatabase db;

    public abstract ContentValues setContentValues(T obj);

    public abstract T setObjectByCursor(Cursor c);

    public abstract String setTableName();

    public abstract String getId(T obj);

    public abstract String getIdColumnName();

    public BaseDAO(){
        this.tableName = setTableName();
    }

    public long inserir(DatabaseHelper db, T obj) {

        //Habilitar o modo ESCRITA
        this.db = db.getWritableDatabase();

        //Especificar os valores para INSERÇÃO
        ContentValues val = setContentValues(obj);

        //Executar a operação de INSERÇÃO
        long id = this.db.insert(
                tableName,
                null,
                val
        );

        return id;
    }

    public long atualizar(DatabaseHelper db, T obj) {
        this.db = db.getWritableDatabase();

        ContentValues val = setContentValues(obj);

        //Definir o ID que será alterado
        String where = getIdColumnName() + " = ?";
        String whereArgs[] = {String.valueOf(getId(obj))};

        //Executar a OPERAÇÃO
        long id = this.db.update(
                tableName,
                val,
                where,
                whereArgs
        );

        return id;
    }

    public void deletar(DatabaseHelper db, int id) {
        this.db = db.getWritableDatabase();
        String where = getIdColumnName() + " = ?";
        String whereArgs[] = new String[]{String.valueOf(id)};

        this.db.delete(
                tableName,    // nome da tabela
                where,                              // where
                whereArgs                           // argumento
        );
    }

    public List<T> listar(DatabaseHelper db) {
        String query = "SELECT * FROM " +  tableName;
        String where[] = null;

        return baseGetList(db, query, where);
    }

    public T pesquisarPorId(DatabaseHelper db, int id) {
        T entity = null;
        String SQL = "SELECT * FROM "+ tableName +" WHERE " + getIdColumnName() + " =?";
        String where[] =  new String[]{String.valueOf(id)};

        this.db = db.getReadableDatabase();

        //Executar a operação de SELEÇÃO
        Cursor c = this.db.rawQuery(SQL,where);

        if (c.moveToFirst()){

            do{
                entity = setObjectByCursor(c);
            }while(c.moveToNext());

        }
        return entity;
    }


    public List<T> baseGetList(DatabaseHelper db, String query, String where[]) {
        List<T> list = new ArrayList<>();

        this.db = db.getReadableDatabase();

        //Executar a operação de SELEÇÃO
        Cursor c = this.db.rawQuery(query,where);

        if (c.moveToFirst()){
            do{
                list.add(setObjectByCursor(c));
            }while(c.moveToNext());
        }

        closeCursor(c);

        return list;
    }

    public void closeCursor(Cursor c){
        //Fechar o cursor
        if (c != null && !c.isClosed()){
            c.close();
        }
    }
}
