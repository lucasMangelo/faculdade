package com.fatec.monedas.Business;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fatec.monedas.R;

public class MovimentacaoViewHolder extends RecyclerView.ViewHolder {

    final TextView txt_descricao;
    final TextView txt_valor;
    final LinearLayout lyt_content;
    final ImageView btn_edit;
    final ImageView btn_delete;
    final TextView movimentacao_id;

    public MovimentacaoViewHolder(View view){
        super(view);

        txt_descricao = (TextView)view.findViewById(R.id.txt_descricao);
        txt_valor = (TextView)view.findViewById(R.id.txt_valor);
        lyt_content = (LinearLayout)view.findViewById(R.id.lyt_content);
        btn_edit = (ImageView)view.findViewById(R.id.btn_edit);
        btn_delete= (ImageView)view.findViewById(R.id.btn_delete);
        movimentacao_id = (TextView)view.findViewById( R.id.movimentacao_id);
    }
}
