package com.fatec.monedas.Controller;

import android.content.ContentValues;
import android.database.Cursor;

import com.fatec.monedas.Model.UsuarioLogado;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UsuarioLogadoDAO extends BaseDAO<UsuarioLogado> implements IDAO<UsuarioLogado>,IDatabaseHelper{
    @Override
    public ContentValues setContentValues(UsuarioLogado obj) {
        ContentValues val = new ContentValues();
        val.put(COL_DATA,  new SimpleDateFormat("dd-MM-yyyy").format(obj.getDate()));
        val.put(COL_ID_USUARIO, obj.getId_usuario());

        return val;
    }

    @Override
    public String setTableName() {
        return TABLE_USUARIOLOGADO;
    }

    @Override
    public String getId(UsuarioLogado obj) {
        return String.valueOf(obj.getId_usuario()) ;
    }

    @Override
    public String getIdColumnName() {
        return COL_ID_USUARIO;
    }

    @Override
    public UsuarioLogado setObjectByCursor(Cursor c) {
        UsuarioLogado obj = new UsuarioLogado();
        obj.setId_usuario( c.getInt(c.getColumnIndex( COL_ID_USUARIO )));

        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date data = format.parse( c.getString( c.getColumnIndex( COL_DATA ) ) );
            obj.setDate(data);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return obj;
    }

    public UsuarioLogado getUsuarioLogado(DatabaseHelper db){
        String sql = "SELECT * FROM " + TABLE_USUARIOLOGADO;
        String where[] = null;

        List<UsuarioLogado> usuario = this.baseGetList( db,sql,where);


        return usuario != null && usuario.size() == 1? usuario.get(0) : null;
    }

    public boolean RegistraUsurioLogado(DatabaseHelper db, UsuarioLogado obj){
        boolean result = true;

        try{
            UsuarioLogado usuario = this.pesquisarPorId( db,obj.getId_usuario());

            long id = usuario == null ? this.inserir( db, obj) : this.atualizar( db,obj);

            if (id == 0 )
                result = false;

        }catch (Exception ex){
            result = false;
        }

        return result;
    }
}
