package com.fatec.monedas.Model;

public class IBOVESPA {
    private String location;

    private String variation;

    private String name;

    private String points;

    public String getLocation ()
    {
        return location;
    }

    public void setLocation (String location)
    {
        this.location = location;
    }

    public String getVariation ()
    {
        return variation;
    }

    public void setVariation (String variation)
    {
        this.variation = variation;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getPoints ()
    {
        return points;
    }

    public void setPoints (String points)
    {
        this.points = points;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [location = "+location+", variation = "+variation+", name = "+name+", points = "+points+"]";
    }

}
