package com.fatec.monedas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fatec.monedas.Controller.DatabaseHelper;
import com.fatec.monedas.Controller.UsuarioLogadoDAO;
import com.fatec.monedas.Model.Movimentacao;
import com.fatec.monedas.Business.MovimentacaoAdapter;
import com.fatec.monedas.Controller.MovimentacaoDAO;
import com.fatec.monedas.Common.DateHeader;
import com.fatec.monedas.Model.UsuarioLogado;

import java.util.ArrayList;
import java.util.List;

public class MovimentacoesActivity extends AppCompatActivity {

    private DatabaseHelper dh;
    private MovimentacaoDAO dao;
    private UsuarioLogadoDAO daoUser;
    private UsuarioLogado user;

    private Spinner spn_tipo;
    private TextView txt_mes;
    private TextView txt_ano;
    private RecyclerView rvc_movimentacoes;
    private String mes ;
    private String ano ;
    private int mes_num ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movimentacoes);

        dh = new DatabaseHelper( this );
        daoUser = new UsuarioLogadoDAO();
        user = daoUser.getUsuarioLogado(dh);
        dao = new MovimentacaoDAO(user);

        String tipo = "";
        int index = 0;

        txt_mes = (TextView)findViewById(R.id.txt_mes);
        txt_ano = (TextView)findViewById(R.id.txt_ano);
        spn_tipo = (Spinner)findViewById(R.id.spn_tipo);
        rvc_movimentacoes = (RecyclerView)findViewById(R.id.rvc_movimentacoes);

        Intent it = getIntent();
        if (it != null){
            tipo = it.getStringExtra("tipo");
            mes = it.getStringExtra("mes");
            ano = it.getStringExtra("ano");
            mes_num = it.getIntExtra( "mes_num" ,0);

            if(tipo != null)
                switch (tipo){
                    case "R":
                        index = 0;
                        break;
                    case "D":
                        index = 1;
                        break;
                }

                if(mes == null && ano == null){
                    DateHeader header = new DateHeader();

                    ano = header.getYear();
                    mes = header.getMonth();
                    mes_num = header.getMonth_num();
                }
        }

        txt_mes.setText(mes);
        txt_ano.setText(ano);

        spn_tipo.setSelection(index);
        spn_tipo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {

                String value = spn_tipo.getSelectedItem().toString();
                RenderMovimentacoesRecyclerView(value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }


    private void RenderMovimentacoesRecyclerView(String opcao){
        List<Movimentacao> lista = new ArrayList<>();

        String tipo = "";

        switch (opcao){
            case "Receita":
                tipo = "R";
                break;
            case "Despesa":
                tipo = "D";
                break;
        }

        Intent it = getIntent();
        if (tipo != "")
            lista = dao.pesquisarPorTipoNoMes(dh, opcao, mes_num);
        else
            lista = dao.listar(dh);

        rvc_movimentacoes.setAdapter(new MovimentacaoAdapter(lista,this));

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,
                false);

        rvc_movimentacoes.setLayoutManager(manager);
    }
}
