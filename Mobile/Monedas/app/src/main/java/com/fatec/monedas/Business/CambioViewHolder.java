package com.fatec.monedas.Business;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fatec.monedas.R;

public class CambioViewHolder extends RecyclerView.ViewHolder {

    final TextView txt_currency;
    final TextView txt_buy;

    public CambioViewHolder(View view){
        super(view);

        txt_currency = (TextView)view.findViewById(R.id.txt_currency);
        txt_buy = (TextView)view.findViewById(R.id.tct_buy);
    }
}
