package com.fatec.monedas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.fatec.monedas.Controller.DatabaseHelper;
import com.fatec.monedas.Controller.UsuarioDAO;
import com.fatec.monedas.Controller.UsuarioLogadoDAO;
import com.fatec.monedas.Model.Usuario;
import com.fatec.monedas.Model.UsuarioLogado;

import java.util.Date;
import java.util.List;

public class CadastrarActivity extends AppCompatActivity {
    private TextView edt_usuario;
    private EditText edt_email;
    private EditText edt_senha;

    private DatabaseHelper dh;
    private UsuarioDAO dao;
    private UsuarioLogadoDAO daoUsuarioLogado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        dh = new DatabaseHelper(this);
        dao = new UsuarioDAO();
        daoUsuarioLogado = new UsuarioLogadoDAO();

        // Botão entrar e verificação do usuário
        Button btn_cadastrar = (Button) findViewById(R.id.btn_cadastrar);
        edt_usuario = (EditText) findViewById(R.id.edt_usuario);
        edt_email = (EditText) findViewById( R.id.edt_email );
        edt_senha = (EditText) findViewById( R.id.edt_senha );


        btn_cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = edt_usuario.getText().toString();
                String senha = edt_senha.getText().toString();
                String email = edt_email.getText().toString();

                List<Usuario> usuarios = dao.pesquisarPorNome(dh,usuario);

                if (validaUsuario(usuario,senha, email)) {
                    Usuario obj = new Usuario(email, usuario,senha);

                    long id = dao.inserir(dh,obj);

                    if(id != 0) {

                        UsuarioLogado logado = new UsuarioLogado();
                        logado.setId_usuario(Integer.parseInt( String.valueOf(id)));
                        logado.setDate( new Date());

                        boolean result = daoUsuarioLogado.RegistraUsurioLogado(dh,logado);

                        if (result) {
                            alerta( "Usuário cadastrado com sucesso!" );
                            startActivity( new Intent( CadastrarActivity.this, DashBoardActivity.class ) );
                            finish();
                        }else{
                            alerta( "Erro ao logar o usuário" );
                        }
                    }else{
                        alerta( "Erro no cadastro" );
                    }
                }

            }
        });
    }
    private void alerta(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    private boolean validaUsuario(String usuario, String senha , String email){
        boolean valid = true;
        String msg = "";
        List<Usuario> usuarios = dao.pesquisarPorNome(dh,usuario);

        if (usuarios != null && usuarios.size() != 0) {
            msg= "Usuário já cadastrado! Informe um novo, por gentileza.";
        }
        else{
            usuarios = dao.pesquisarPorEmail(dh, email);

            if (usuarios != null && usuarios.size() != 0)
                msg = "Email já cadastrado! Informe um novo, por gentileza.";
            else if( senha == "")
                msg = "Senha não informada!";
        }

        if(!valid)
            alerta(msg);

        return valid;
    }
}
