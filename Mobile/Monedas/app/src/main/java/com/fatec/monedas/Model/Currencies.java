package com.fatec.monedas.Model;

public class Currencies {
    private EUR EUR;

    private String source;

    private ARS ARS;

    private BTC BTC;

    private USD USD;

    private GBP GBP;

    public EUR getEUR ()
    {
        return EUR;
    }

    public void setEUR (EUR EUR)
    {
        this.EUR = EUR;
    }

    public String getSource ()
    {
        return source;
    }

    public void setSource (String source)
    {
        this.source = source;
    }

    public ARS getARS ()
    {
        return ARS;
    }

    public void setARS (ARS ARS)
    {
        this.ARS = ARS;
    }

    public BTC getBTC ()
    {
        return BTC;
    }

    public void setBTC (BTC BTC)
    {
        this.BTC = BTC;
    }

    public USD getUSD ()
    {
        return USD;
    }

    public void setUSD (USD USD)
    {
        this.USD = USD;
    }

    public GBP getGBP ()
    {
        return GBP;
    }

    public void setGBP (GBP GBP)
    {
        this.GBP = GBP;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [EUR = "+EUR+", source = "+source+", ARS = "+ARS+", BTC = "+BTC+", USD = "+USD+", GBP = "+GBP+"]";
    }
}
