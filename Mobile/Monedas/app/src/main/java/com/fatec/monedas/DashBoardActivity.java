package com.fatec.monedas;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fatec.monedas.Common.DateHeader;
import com.fatec.monedas.Common.Utils;
import com.fatec.monedas.Controller.DatabaseHelper;
import com.fatec.monedas.Controller.FixerService;
import com.fatec.monedas.Controller.MovimentacaoDAO;
import com.fatec.monedas.Controller.UsuarioLogadoDAO;
import com.fatec.monedas.Model.UsuarioLogado;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DashBoardActivity extends AppCompatActivity {

    private DatabaseHelper dh;
    private MovimentacaoDAO dao;

    private LinearLayout date;
    private LinearLayout lyt_receita;
    private LinearLayout lyt_despesa;

    private  TextView txt_saldo_receita;
    private  TextView txt_saldo_despesas;
    private  TextView txt_saldo;

    private TextView txt_mes;
    private TextView txt_ano;
    private FloatingActionButton ftb_add;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private ImageButton imgBtnVoltar;

    private UsuarioLogadoDAO daoUsuarioLogado;

    private UsuarioLogadoDAO daoUser;
    private UsuarioLogado user;

    private int mes;

    private Button btnConsultar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        dh = new DatabaseHelper(this);
        daoUser = new UsuarioLogadoDAO();
        user = daoUser.getUsuarioLogado(dh);
        dao = new MovimentacaoDAO(user);

        date = (LinearLayout)findViewById(R.id.date);
        lyt_receita = (LinearLayout)findViewById(R.id.lyt_receita) ;
        lyt_despesa = (LinearLayout)findViewById(R.id.lyt_despesa) ;
        txt_mes = (TextView)findViewById(R.id.txt_mes);
        txt_ano = (TextView)findViewById(R.id.txt_ano);
        ftb_add = (FloatingActionButton)findViewById(R.id.add);
        txt_saldo_receita = (TextView)findViewById( R.id.txt_saldo_receita);
        txt_saldo_despesas = (TextView)findViewById( R.id.txt_saldo_despesas);
        txt_saldo = (TextView)findViewById( R.id.txt_saldo ) ;
        imgBtnVoltar = (ImageButton) findViewById(R.id.imgBtnVoltar);

        btnConsultar = (Button) findViewById(R.id.btnConsultar);

        Date date = new Date();
        setDateHeader(date);

        int mes = date.getMonth() + 1;

        Double totalReceita = dao.retornaTotalBalancoMesETipo(dh,mes, "Receita");
        Double totalDespesa = dao.retornaTotalBalancoMesETipo(dh,mes, "Despesa");

        txt_saldo_receita.setText( Utils.showDouble(totalReceita));
        txt_saldo_despesas.setText( Utils.showDouble(totalDespesa));
        txt_saldo.setText(Utils.showDouble(totalReceita - totalDespesa) );

        setDetailEvents();
        setDatePickerDialogEvent();
        setAddButtonEvent();

        imgBtnVoltar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                AlertDialog.Builder alerta = new AlertDialog.Builder(DashBoardActivity.this);

                alerta.setTitle("Aviso!");
                alerta.setIcon(R.drawable.ic_warning);
                alerta.setMessage("Você deseja sair?");
                alerta.setCancelable(false);

                alerta.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                alerta.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                    }
                });

                AlertDialog alertDialog = alerta.create();
                alertDialog.show();
            }
        });

        btnConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(
                        new Intent(
                                getApplicationContext(),
                                CambioActivity.class)
                );
            }
        });
    }


    public void ShowMovimentacoesActivity(String tipo){
        // Definir a estratégia de navegação

        Intent it = new Intent(
                getApplicationContext(), // Tela Origem
                MovimentacoesActivity .class // Tela Destino
        );

        it.putExtra("tipo",tipo); // envia o tipo para a próxima tela
        it.putExtra("mes", txt_mes.getText());
        it.putExtra("ano", txt_ano.getText());
        it.putExtra( "mes_num",mes);

        startActivity(it);
    }

    public void setAddButtonEvent(){
        ftb_add.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent it = new Intent(
                        getApplicationContext(), // Tela Origem
                        MovimentacaoActivity .class // Tela Destino
                );
                startActivity(it);
            }
        });
    }

    public void setDetailEvents(){
        lyt_receita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tipo = "R";
                ShowMovimentacoesActivity(tipo);
            }
        });

        lyt_despesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tipo = "D";
                ShowMovimentacoesActivity(tipo);
            }
        });
    }

    public void setDateHeader(Date date){
        DateHeader dateHeader = new DateHeader(date);
        txt_mes.setText(dateHeader.getMonth());
        txt_ano.setText(dateHeader.getYear());
        mes = dateHeader.getMonth_num();
    }

    public void setDatePickerDialogEvent(){

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        DashBoardActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog,
                        mDateSetListener,
                        year, month, day);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month++;
                try {
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    String test = String.format("%d/%d/%d", day, month, year);
                    Date date = format.parse(test);
                    setDateHeader(date);
                }
                catch (Exception ex){

                }
            }
        };
    }

    private DatePickerDialog customDatePicker() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dpd = new DatePickerDialog(this, mDateSetListener,
                year, month, day);
        try {
            Field[] datePickerDialogFields = dpd.getClass().getDeclaredFields();
            for (Field datePickerDialogField : datePickerDialogFields) {
                if (datePickerDialogField.getName().equals("mDatePicker")) {
                    datePickerDialogField.setAccessible(true);
                    DatePicker datePicker = (DatePicker) datePickerDialogField
                            .get(dpd);
                    Field datePickerFields[] = datePickerDialogField.getType()
                            .getDeclaredFields();
                    for (Field datePickerField : datePickerFields) {
                        if ("mDayPicker".equals(datePickerField.getName())
                                || "mDaySpinner".equals(datePickerField
                                .getName())) {
                            datePickerField.setAccessible(true);
                            Object dayPicker = new Object();
                            dayPicker = datePickerField.get(datePicker);
                            ((View) dayPicker).setVisibility(View.GONE);
                        }
                    }
                }

            }
        } catch (Exception ex) {
        }
        return dpd;
    }


}
