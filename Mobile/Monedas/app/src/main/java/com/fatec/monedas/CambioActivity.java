package com.fatec.monedas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.fatec.monedas.Business.CambioAdapter;
import com.fatec.monedas.Business.MovimentacaoAdapter;
import com.fatec.monedas.Controller.FixerService;
import com.fatec.monedas.Model.BTC;
import com.fatec.monedas.Model.Conversion;
import com.fatec.monedas.Model.Currencies;
import com.fatec.monedas.Model.EUR;
import com.fatec.monedas.Model.ICurrency;
import com.fatec.monedas.Model.Results;
import com.fatec.monedas.Model.USD;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CambioActivity extends AppCompatActivity {

    // Conexão API
    private static final String TAG = "monedas";
    public static final String BASE_URL = "https://api.hgbrasil.com/finance/";
    private static Retrofit retrofit = null;

    RecyclerView rvc_cambio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambio);

        rvc_cambio = (RecyclerView) findViewById(R.id.rvc_cambio);

        getValores();
    }
    public void getValores(){
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        // Criando a interface da API
        FixerService fixer = retrofit.create(FixerService.class);

        Call<Conversion> call = fixer.getCambio();

        // Fazendo a requisição
        call.enqueue(new Callback<Conversion>() {
            @Override
            public void onResponse(Call<Conversion> call, Response<Conversion> response) {
                Conversion valores = response.body();
                Results results = valores.getResults();

                Currencies currencies = results.getCurrencies();

                USD usd = currencies.getUSD();
                EUR eur = currencies.getEUR();
                BTC btc = currencies.getBTC();

                // Criando um array pros valores de conversão
                List<ICurrency> lista = new ArrayList<ICurrency>();

                lista.add(usd);
                lista.add(eur);
                lista.add(btc);

                rvc_cambio.setAdapter(new CambioAdapter(lista, getApplicationContext()));

                RecyclerView.LayoutManager manager = new LinearLayoutManager(
                        getApplicationContext(),
                        LinearLayoutManager.VERTICAL,
                        false);

                rvc_cambio.setLayoutManager(manager);
            }

            @Override
            public void onFailure(Call<Conversion> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
