package com.fatec.monedas.Model;

public class Omnitrade {

    private String last;

    private String sell;

    private String buy;

    private String variation;

    private String name;

    private String[] format;

    public String getLast ()
    {
        return last;
    }

    public void setLast (String last)
    {
        this.last = last;
    }

    public String getSell ()
    {
        return sell;
    }

    public void setSell (String sell)
    {
        this.sell = sell;
    }

    public String getBuy ()
    {
        return buy;
    }

    public void setBuy (String buy)
    {
        this.buy = buy;
    }

    public String getVariation ()
    {
        return variation;
    }

    public void setVariation (String variation)
    {
        this.variation = variation;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String[] getFormat ()
    {
        return format;
    }

    public void setFormat (String[] format)
    {
        this.format = format;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [last = "+last+", sell = "+sell+", buy = "+buy+", variation = "+variation+", name = "+name+", format = "+format+"]";
    }
}
