
package com.fatec.monedas.Controller;

public interface IDatabaseHelper {

    //Versão da base de dados
    public static final int DATABASE_VERSION = 7;

    //Nome da base de dados
    public static final String DATABASE_NAME="monedas.db";

    //Nomes das tabelas
    public static final String TABLE_USUARIO="usuario";
    public static final String TABLE_USUARIOLOGADO = "usuariologado";
    public static final String TABLE_MOVIMENTACAO="movimentacao";


    //Nomes das colunas (comuns)
    public static final String COL_ID="id";
    public static final String COL_NOME="nome";

    //Tabela Movimentacao = colunas específicas
    public static final String COL_DESCRICAO="descricao";
    public static final String COL_TIPO="tipo";
    public static final String COL_DATA="data";
    public static final String COL_VALOR="valor";
    public static final String COL_LOCALIZACAO="localizacao";

    // Tabela Usuario = colunas específicas
    public static final String COL_SENHA="senha";
    public static final String COL_EMAIL="email";

    // Tabela Usuario Logado = colunas específicas
    public static final String COL_ID_USUARIO="id_usuario";

    // INSTRUÇÕES para CRIAÇÃO DAS TABELAS
    //
    public static final String CREATE_TABLE_MOVIMENTACAO =
            "CREATE TABLE " + TABLE_MOVIMENTACAO + " (" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_DESCRICAO + " TEXT, " +
                    COL_VALOR + " REAL, " +
                    COL_DATA + " date," +
                    COL_TIPO + " TEXT," +
                    COL_LOCALIZACAO + " TEXT NULL," +
                    COL_ID_USUARIO + " INTEGER, "+
                    " FOREIGN KEY ("+COL_ID_USUARIO+ ") REFERENCES "+ TABLE_USUARIO +" ("+ COL_ID +")" +
                    ")";

    public static final String CREATE_TABLE_USUARIO =
            "CREATE TABLE " + TABLE_USUARIO + " (" +
                    COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    COL_NOME + " TEXT," +
                    COL_SENHA + " TEXT," +
                    COL_EMAIL + " TEXT" +
                    ")";

    public static final String CREATE_TABLE_USUARIOLOGADO =
            "CREATE TABLE " + TABLE_USUARIOLOGADO + " ( " +
                    COL_ID_USUARIO + " INTEGER PRIMARY KEY, " +
                    COL_DATA + " date," +
                    "FOREIGN KEY ("+COL_ID_USUARIO+ ") REFERENCES "+TABLE_USUARIO+" ("+ COL_ID +")" +
                    ")";
}
