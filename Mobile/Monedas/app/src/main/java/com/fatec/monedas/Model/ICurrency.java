package com.fatec.monedas.Model;

public interface ICurrency {

    String getBuy ();

    void setBuy (String buy);

    String getVariation ();

    void setVariation (String variation);

    String getName ();

    void setName (String name);
}
