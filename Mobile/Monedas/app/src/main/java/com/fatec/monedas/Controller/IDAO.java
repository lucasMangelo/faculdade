package com.fatec.monedas.Controller;


import java.util.List;

public interface IDAO<T> {

    public long inserir(DatabaseHelper db, T obj);
    public long atualizar(DatabaseHelper db, T obj);
    public void deletar(DatabaseHelper db, int id);
    public List<T> listar(DatabaseHelper db);
    public T pesquisarPorId(DatabaseHelper db, int id);

}
