package com.fatec.monedas.Model;

public class BTC implements ICurrency {
    private String sell;

    private String buy;

    private String variation;

    private String name;

    public String getSell ()
    {
        return sell;
    }

    public void setSell (String sell)
    {
        this.sell = sell;
    }

    public String getBuy ()
    {
        return buy;
    }

    public void setBuy (String buy)
    {
        this.buy = buy;
    }

    public String getVariation ()
    {
        return variation;
    }

    public void setVariation (String variation)
    {
        this.variation = variation;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [sell = "+sell+", buy = "+buy+", variation = "+variation+", name = "+name+"]";
    }
}
