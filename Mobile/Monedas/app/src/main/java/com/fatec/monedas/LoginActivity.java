package com.fatec.monedas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.fatec.monedas.Controller.DatabaseHelper;
import com.fatec.monedas.Controller.MovimentacaoDAO;
import com.fatec.monedas.Controller.UsuarioDAO;
import com.fatec.monedas.Controller.UsuarioLogadoDAO;
import com.fatec.monedas.Model.Usuario;
import com.fatec.monedas.Model.UsuarioLogado;

import java.util.Date;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    private TextView edt_usuario;
    private EditText edt_senha;
    private TextView txt_esqueceu_senha;
    private TextView txt_cadastrar;
    private Button btnEntrar;

    private DatabaseHelper dh;
    private UsuarioDAO dao;
    private UsuarioLogadoDAO daoUsuarioLogado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        dh = new DatabaseHelper(this);
        dao = new UsuarioDAO();
        daoUsuarioLogado = new UsuarioLogadoDAO();

        // Botão entrar e verificação do usuário
        btnEntrar = (Button) findViewById(R.id.btnEntrar);
        edt_usuario = (EditText) findViewById(R.id.edtUsuario);
        edt_senha = (EditText) findViewById(R.id.edtSenha);
        txt_esqueceu_senha = (TextView)findViewById( R.id.txt_esqueceu_senha);
        txt_cadastrar = (TextView) findViewById(R.id.txt_cadastrar);

        Intent it = getIntent();
        if (it != null) {
            String usuario = it.getStringExtra( "usuario" );

            if (usuario != null)
                edt_usuario.setText(usuario);
        }

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usuario = edt_usuario.getText().toString();
                String senha = edt_senha.getText().toString();
                List<Usuario> users = dao.pesquisarPorNomeSenha(dh,usuario,senha);

                if(users != null && users.size() == 1){


                    UsuarioLogado obj = new UsuarioLogado();
                    obj.setId_usuario( users.get(0).getId());
                    obj.setDate( new Date());

                    boolean result =  daoUsuarioLogado.RegistraUsurioLogado(dh,obj);

                    if(result) {
                        alerta( "Usuário conectado com sucesso!" );
                        Intent it = new Intent( LoginActivity.this, DashBoardActivity.class );
                        startActivity( it );
                        finish();
                    }else{
                        alerta( "Falha ao logar o usuário" );
                    }
                }
                else{
                    alerta("Usuário ou senha incorretos!");
                }
            }
        });

        // Tela para recuperar a senha
        txt_esqueceu_senha.setOnClickListener( new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent it = new Intent(LoginActivity.this, EsqueceuSenhaActivity.class);
                String usuario = edt_usuario.getText().toString();

                it.putExtra("usuario",usuario);
                startActivity(it);
            }
        });

        // Tela para cadastrar
        txt_cadastrar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(getApplicationContext(),CadastrarActivity.class));
            }
        });

        // Botão "Sobre"
        ImageButton imgBtnInfo = (ImageButton) findViewById(R.id.imgBtnInfo);

        imgBtnInfo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(getApplicationContext(),SobreActivity.class));
            }
        });
    }
    private void alerta(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }
}
