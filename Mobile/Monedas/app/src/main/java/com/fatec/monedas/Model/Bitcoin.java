package com.fatec.monedas.Model;

public class Bitcoin {
    private Foxbit foxbit;

    private Coinbase coinbase;

    private Omnitrade omnitrade;

    private Bitstamp bitstamp;

    private Xdex xdex;

    private Mercadobitcoin mercadobitcoin;

    private Blockchain_info blockchain_info;

    public Foxbit getFoxbit ()
    {
        return foxbit;
    }

    public void setFoxbit (Foxbit foxbit)
    {
        this.foxbit = foxbit;
    }

    public Coinbase getCoinbase ()
    {
        return coinbase;
    }

    public void setCoinbase (Coinbase coinbase)
    {
        this.coinbase = coinbase;
    }

    public Omnitrade getOmnitrade ()
    {
        return omnitrade;
    }

    public void setOmnitrade (Omnitrade omnitrade)
    {
        this.omnitrade = omnitrade;
    }

    public Bitstamp getBitstamp ()
    {
        return bitstamp;
    }

    public void setBitstamp (Bitstamp bitstamp)
    {
        this.bitstamp = bitstamp;
    }

    public Xdex getXdex ()
    {
        return xdex;
    }

    public void setXdex (Xdex xdex)
    {
        this.xdex = xdex;
    }

    public Mercadobitcoin getMercadobitcoin ()
    {
        return mercadobitcoin;
    }

    public void setMercadobitcoin (Mercadobitcoin mercadobitcoin)
    {
        this.mercadobitcoin = mercadobitcoin;
    }

    public Blockchain_info getBlockchain_info ()
    {
        return blockchain_info;
    }

    public void setBlockchain_info (Blockchain_info blockchain_info)
    {
        this.blockchain_info = blockchain_info;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [foxbit = "+foxbit+", coinbase = "+coinbase+", omnitrade = "+omnitrade+", bitstamp = "+bitstamp+", xdex = "+xdex+", mercadobitcoin = "+mercadobitcoin+", blockchain_info = "+blockchain_info+"]";
    }
}
