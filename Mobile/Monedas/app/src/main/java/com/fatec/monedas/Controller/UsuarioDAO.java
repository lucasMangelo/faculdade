package com.fatec.monedas.Controller;

import android.content.ContentValues;
import android.database.Cursor;
import com.fatec.monedas.Model.Usuario;

import java.util.ArrayList;
import java.util.List;

public class UsuarioDAO  extends BaseDAO<Usuario> implements IDAO<Usuario>,IDatabaseHelper {

    @Override
    public ContentValues setContentValues(Usuario obj) {
        ContentValues val = new ContentValues();
        val.put(COL_EMAIL, obj.getEmail());
        val.put(COL_NOME, obj.getNome());
        val.put(COL_SENHA, obj.getSenha());

        return val;
    }

    @Override
    public String setTableName() {
        return TABLE_USUARIO;
    }

    @Override
    public String getId(Usuario obj) {
        return String.valueOf(obj.getId()) ;
    }

    @Override
    public String getIdColumnName() {
        return COL_ID;
    }

    @Override
    public Usuario setObjectByCursor(Cursor c) {
        Usuario obj = new Usuario();
        obj.setId( c.getInt(c.getColumnIndex( "id" )));
        obj.setEmail(c.getString(c.getColumnIndex( "email" )));
        obj.setNome(c.getString(c.getColumnIndex( "nome" )));
        obj.setSenha( c.getString(c.getColumnIndex( "senha" )));

        return obj;
    }

    public List<Usuario> pesquisarPorNome(DatabaseHelper db, String nome) {
        String query = "SELECT * FROM "+ TABLE_USUARIO +
                        " WHERE " + IDatabaseHelper.COL_NOME + "=?";
        String where [] = new String[]{String.valueOf(nome)};

        return baseGetList(db,query,where);
    }

    public List<Usuario> pesquisarPorEmail(DatabaseHelper db, String email) {
        String query = "SELECT * FROM "+ TABLE_USUARIO +
                " WHERE " + IDatabaseHelper.COL_EMAIL + " =?";
        String where [] = new String[]{String.valueOf(email)};

        return baseGetList(db,query,where);
    }

    public List<Usuario> pesquisarPorNomeSenha(DatabaseHelper db, String nome, String senha) {
        String query = "SELECT * FROM "+ TABLE_USUARIO +
                " WHERE " + IDatabaseHelper.COL_NOME + "=?" +
                " AND " + COL_SENHA + " =?";

        List<Usuario> list = new ArrayList<>( );

        String where [] = new String[]{nome, senha};

        try {
            list = baseGetList( db, query, where );
        }catch (Exception ex){
            String msg = ex.getMessage();
        }

        return list;
    }
}
