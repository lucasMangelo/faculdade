package com.fatec.monedas.Model;

import java.util.Date;

public class UsuarioLogado {
    private int id_usuario;
    private Date date;

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
