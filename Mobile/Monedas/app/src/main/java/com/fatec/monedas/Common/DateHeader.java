package com.fatec.monedas.Common;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHeader {

    private String Month;

    private String Year;

    private int Month_num;
    private int Year_num;

    public String getMonth() {
        return Month;
    }

    public void setMonth(String month) {
        Month = month;
    }

    public String getYear() {
        return Year;
    }

    public void setYear(String year) {
        Year = year;
    }

    public DateHeader(){
        Date data = new Date();
        setDate(data);
    }

    public DateHeader(Date date) {
        setDate(date);
    }

    public void setDate(Date date){
        if(date != null){
            setMonth(getMonth(date.getMonth()));
            setMonth_num(date.getMonth() + 1);
            setYear(getYear(date));
            setYear_num(Integer.parseInt( this.getYear()));
        }
    }

    public static String getMonth(int month){
        month++;

        String description = "";
        switch (month){
            case 1:
                description = "Janeiro";
                break;
            case 2:
                description = "Fevereior";
                break;
            case 3:
                description = "Março";
                break;
            case 4:
                description = "Abril";
                break;
            case 5:
                description = "Maio";
                break;
            case 6:
                description = "Junho";
                break;
            case 7:
                description = "Julho";
                break;
            case 8:
                description = "Agosto";
                break;
            case 9:
                description = "Setembro";
                break;
            case 10:
                description = "Outubro";
                break;
            case 11:
                description = "Novembro";
                break;
            case 12:
                description = "Dezembro";
                break;
        }

        return description;
    }

    public String getYear(Date date){
        SimpleDateFormat format = new SimpleDateFormat("yyyy");
        return format.format(date);
    }

    public int getMonth_num() {
        return Month_num;
    }

    public void setMonth_num(int month_num) {
        Month_num = month_num;
    }

    public int getYear_num() {
        return Year_num;
    }

    public void setYear_num(int year_num) {
        Year_num = year_num;
    }
}
