package com.fatec.monedas.Model;

public class Stocks {
    private IBOVESPA IBOVESPA;

    private NASDAQ NASDAQ;

    private CAC CAC;

    private NIKKEI NIKKEI;

    public IBOVESPA getIBOVESPA ()
    {
        return IBOVESPA;
    }

    public void setIBOVESPA (IBOVESPA IBOVESPA)
    {
        this.IBOVESPA = IBOVESPA;
    }

    public NASDAQ getNASDAQ ()
    {
        return NASDAQ;
    }

    public void setNASDAQ (NASDAQ NASDAQ)
    {
        this.NASDAQ = NASDAQ;
    }

    public CAC getCAC ()
    {
        return CAC;
    }

    public void setCAC (CAC CAC)
    {
        this.CAC = CAC;
    }

    public NIKKEI getNIKKEI ()
    {
        return NIKKEI;
    }

    public void setNIKKEI (NIKKEI NIKKEI)
    {
        this.NIKKEI = NIKKEI;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [IBOVESPA = "+IBOVESPA+", NASDAQ = "+NASDAQ+", CAC = "+CAC+", NIKKEI = "+NIKKEI+"]";
    }
}
