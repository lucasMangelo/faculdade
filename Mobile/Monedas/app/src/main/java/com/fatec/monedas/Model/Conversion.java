package com.fatec.monedas.Model;

public class Conversion {
    private String valid_key;

    private Results results;

    private String from_cache;

    private String execution_time;

    private String by;

    public String getValid_key ()
    {
        return valid_key;
    }

    public void setValid_key (String valid_key)
    {
        this.valid_key = valid_key;
    }

    public Results getResults ()
    {
        return results;
    }

    public void setResults (Results results)
    {
        this.results = results;
    }

    public String getFrom_cache ()
    {
        return from_cache;
    }

    public void setFrom_cache (String from_cache)
    {
        this.from_cache = from_cache;
    }

    public String getExecution_time ()
    {
        return execution_time;
    }

    public void setExecution_time (String execution_time)
    {
        this.execution_time = execution_time;
    }

    public String getBy ()
    {
        return by;
    }

    public void setBy (String by)
    {
        this.by = by;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [valid_key = "+valid_key+", results = "+results+", from_cache = "+from_cache+", execution_time = "+execution_time+", by = "+by+"]";
    }
}
