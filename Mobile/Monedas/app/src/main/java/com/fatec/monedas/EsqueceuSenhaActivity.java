package com.fatec.monedas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fatec.monedas.Common.Utils;

public class EsqueceuSenhaActivity extends AppCompatActivity {

    private EditText edt_usuario;
    private EditText edt_email;
    private Button btn_recuperar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_esqueceu_senha );

        edt_usuario = (EditText)findViewById(R.id.edt_usuario);
        edt_email = (EditText)findViewById( R.id.edt_email);
        btn_recuperar = (Button)findViewById( R.id.btn_recuperar );

        btn_recuperar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isValid = true;

                if(!isValidateEmail()){
                    isValid = false;
                }

                if(!isValidateUsuario() && isValid){
                    isValid = false;
                }

                if (isValid) {
                    Utils.showToast( "Te mandamos uma nova senha !\nVerifique seu e-mail :)", getApplicationContext() );

                    Intent it = new Intent(EsqueceuSenhaActivity.this, LoginActivity.class);

                    String usuario = edt_usuario.getText().toString();

                    it.putExtra("usuario",usuario);

                    startActivity(it);
                    finish();
                }
            }
        });
    }

    public boolean isValidateEmail(){
        boolean valid = true;

        String text = edt_email.getText().toString();

        if(text.isEmpty())
            valid = false;

        return valid;
    }

    public boolean isValidateUsuario(){
        boolean valid = true;

        String text = edt_usuario.getText().toString();

        if(text.isEmpty())
            valid = false;

        return valid;
    }
}
