package com.fatec.monedas;

import android.app.DatePickerDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;

import com.fatec.monedas.Common.Utils;
import com.fatec.monedas.Controller.DatabaseHelper;
import com.fatec.monedas.Controller.MovimentacaoDAO;
import com.fatec.monedas.Controller.UsuarioLogadoDAO;
import com.fatec.monedas.Model.Movimentacao;
import com.fatec.monedas.Model.UsuarioLogado;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MovimentacaoActivity extends AppCompatActivity {

    private EditText edt_data;
    private EditText edt_valor;
    private EditText edt_descricao;
    private RadioGroup rdg_tipo;
    private Button btn_salvar;

    private int id_movimentacao;

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private static String DESCRICAO = "Descrição";
    private static String VALOR = "Valor";

    private DatabaseHelper dh;
    private MovimentacaoDAO dao;
    private UsuarioLogadoDAO daoUser;
    private UsuarioLogado user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movimentacao);

        dh = new DatabaseHelper(this);
        daoUser = new UsuarioLogadoDAO();
        user = daoUser.getUsuarioLogado(dh);
        dao = new MovimentacaoDAO(user);

        edt_data = (EditText)findViewById(R.id.edt_data);
        edt_valor = (EditText)findViewById(R.id.edt_valor);
        edt_descricao = (EditText)findViewById(R.id.edt_descricao);
        btn_salvar = (Button)findViewById(R.id.btn_salvar);
        rdg_tipo = (RadioGroup) findViewById( R.id.rdg_tipo);


        Intent it = getIntent();
        if (it != null) {
            String descricao = it.getStringExtra( "descricao" ),
                     valor = it.getStringExtra( "valor" ),
                     data = it.getStringExtra( "data" ),
                     id = it.getStringExtra( "id" );

            if(id != null) {
                id_movimentacao = Integer.parseInt( id );

                Movimentacao item = dao.pesquisarPorId(dh,id_movimentacao);
                if(item  != null){
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

                    descricao = item.getDescricao();
                    valor = item.getValor().toString();
                    data = format.format(item.getData());
                }

            }

            if (descricao != null)
                edt_descricao.setText(descricao);

            if(valor != null)
                edt_valor.setText(valor);

            if(data != null)
                edt_data.setText(data);
            else{
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                Date datEdt = new Date();
                edt_data.setText(format.format(datEdt));

            }
        }
        setBtnSalvarClickEvent();
        setDatePickerDialogEvent();

    }

    public void setBtnSalvarClickEvent(){
        btn_salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean isValid = true;
                if (!isValidateValor()) {
                    isValid = false;
                    Utils.showToast("Informe o valor.",getApplicationContext());
                }

                if (!isValidateDescricao() && isValid) {
                    isValid = false;
                    Utils.showToast("Informe a descrição.",getApplicationContext());
                }

                String tipo = "";

                if(isValid){

                    switch(rdg_tipo.getCheckedRadioButtonId()){
                        case R.id.rdb_despesa:
                            tipo = "Despesa";
                            break;
                        case R.id.rdb_receita:
                            tipo = "Receita";
                            break;
                    }

                    Movimentacao movimentacao = new Movimentacao( );
                    movimentacao.setValor( Double.parseDouble(  edt_valor.getText().toString()  ));
                    movimentacao.setDescricao( edt_descricao.getText().toString());
                    movimentacao.setTipo(tipo );
                    movimentacao.setId(id_movimentacao);


                    try {
                        movimentacao.setData(  new SimpleDateFormat("dd/MM/yyyy").parse(edt_data.getText().toString()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    try {
                        long id = 0;

                        String titulo = tipo;

                        if(id_movimentacao == 0) {
                            id = dao.inserir( dh, movimentacao );
                            titulo = " criada com sucesso.";
                        }
                        else{

                            id = dao.atualizar( dh, movimentacao );
                            titulo = " atualizada com sucesso.";
                        }

                        if (id != 0) {
                            criarNotificacao( titulo, "Veja detalhes da movimentação de hoje." );

                            Intent it = new Intent(
                                    getApplicationContext(), // Tela Origem
                                    DashBoardActivity.class // Tela Destino
                            );
                            startActivity( it );

                            Utils.showToast( titulo, getApplicationContext() );
                        }
                    }
                    catch (Exception ex){
                        criarNotificacao( "Erro", ex.getMessage());
                    }
                }
            }
        });
    }

    public void setDatePickerDialogEvent(){

        edt_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                try {
                    Calendar cal = Calendar.getInstance();

                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    SimpleDateFormat format_year = new SimpleDateFormat("yyyy");
                    SimpleDateFormat format_month = new SimpleDateFormat("MM");
                    SimpleDateFormat format_day = new SimpleDateFormat("dd");

                    String date_txt = edt_data.getText().toString();

                    Date date = format.parse(date_txt);

                    int year = Integer.parseInt(format_year.format(date));
                    int month = Integer.parseInt(format_month.format(date));
                    int day = Integer.parseInt(format_day.format(date));

                    DatePickerDialog dialog = new DatePickerDialog(
                            MovimentacaoActivity.this,
                            android.R.style.Theme_Holo_Light_Dialog,
                            mDateSetListener,
                            year, month, day);

                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    dialog.show();
                }
                catch (Exception ex){

                }
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month++;
                try {
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    String date = String.format("%d/%d/%d", day, month, year);
                    edt_data.setText(date);
                }
                catch (Exception ex){

                }
            }
        };
    }

    public boolean isValidateValor(){
        boolean valid = true;

        String text = edt_valor.getText().toString();

        if(text.isEmpty())
            valid = false;

        return valid;
    }

    public boolean isValidateDescricao(){
        boolean valid = true;

        String text = edt_descricao.getText().toString();

        if(text.isEmpty())
            valid = false;

        return valid;
    }

    private void criarNotificacao(String titulo, String texto){

        // 01. Definir as propriedades da Notificação
        final int NOTIFICATION_ID = 123;
        final String CHANNEL_ID = "Notificação";

        // 02. Instanciar o gerenciador de notificações
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);


        // 03. Definir um Canal de Notificação para API >= 28
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel canal = new NotificationChannel(CHANNEL_ID, "canal", importance);
            canal.setDescription("Canal de Notificação");
            canal.enableLights(true);
            canal.setLightColor(Color.RED);
            canal.enableVibration(true);
            canal.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            canal.setShowBadge(true);
            notificationManager.createNotificationChannel(canal);
        }

        // 04. Especificar o ícone, o título e a mensagem da notificação
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.monedas)
                .setContentTitle(titulo)
                .setStyle(
                        new NotificationCompat.BigTextStyle().bigText(texto)
                )
                .setContentText(texto);

        // 05. Definir qual Atividade será chamada quando o usuário clicar na notificação
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MovimentacoesActivity.class);
        stackBuilder.addNextIntent(new Intent(this, MovimentacoesActivity.class));
        PendingIntent it = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(it);

        // 06. Exibir a notificação
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

}
