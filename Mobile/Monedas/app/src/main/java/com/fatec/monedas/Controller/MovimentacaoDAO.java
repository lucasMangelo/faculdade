package com.fatec.monedas.Controller;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.fatec.monedas.Model.Movimentacao;
import com.fatec.monedas.Model.UsuarioLogado;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MovimentacaoDAO extends BaseDAO<Movimentacao> implements IDAO<Movimentacao>,IDatabaseHelper {

    private UsuarioLogado usuarioLogado;

    public MovimentacaoDAO(UsuarioLogado usuarioLogado){
        this.usuarioLogado = usuarioLogado;
    }

    @Override
    public ContentValues setContentValues(Movimentacao obj) {
        ContentValues val = new ContentValues();
        val.put(COL_DESCRICAO, obj.getDescricao());
        val.put(COL_TIPO, obj.getTipo());
        val.put(COL_LOCALIZACAO, obj.getLocalizacao());
        val.put(COL_VALOR, obj.getValor());
        val.put(COL_DATA, new SimpleDateFormat("yyyy-MM-dd").format(obj.getData()));
        val.put(COL_ID_USUARIO, usuarioLogado.getId_usuario());

        return val;
    }

    @Override
    public String setTableName() {
        return TABLE_MOVIMENTACAO;
    }

    @Override
    public String getId(Movimentacao obj) {
        return  String.valueOf(obj.getId());
    }

    @Override
    public String getIdColumnName() {
        return COL_ID;
    }

    public Movimentacao setObjectByCursor(Cursor c){
        Movimentacao movimentacao = new Movimentacao();
        movimentacao.setId(c.getInt(c.getColumnIndex( COL_ID )));
        movimentacao.setDescricao(c.getString(c.getColumnIndex( COL_DESCRICAO)));
        movimentacao.setValor(c.getDouble(c.getColumnIndex( COL_VALOR )));
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            Date data = format.parse( c.getString( c.getColumnIndex( COL_DATA ) ) );

            movimentacao.setData( data);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        movimentacao.setTipo(c.getString(c.getColumnIndex( COL_TIPO)));
        movimentacao.setLocalizacao( c.getString(c.getColumnIndex( COL_LOCALIZACAO) ));
        movimentacao.setId_usuario(c.getInt(c.getColumnIndex( COL_ID_USUARIO)));

        return movimentacao;
    }

    public void closeCursor(Cursor c){
        //Fechar o cursor
        if (c != null && !c.isClosed()){
            c.close();
        }
    }

    public List<Movimentacao> pesquisarPorTipoNoMes(DatabaseHelper db, String tipo ,int mes) {
        List<Movimentacao> list = new ArrayList<>();
        String SQL = "SELECT * FROM movimentacao WHERE "
                    + IDatabaseHelper.COL_TIPO + "=?"
                    + " AND strftime('%m',data) =?"
                    + " AND " + COL_ID_USUARIO + " =?";

        String where[] =  new String[]{String.valueOf(tipo), mes < 10 ? "0" : "" + String.valueOf(mes), String.valueOf(usuarioLogado.getId_usuario())};

        return baseGetList( db, SQL,where );
    }

    public Double retornaTotalBalancoMesETipo(DatabaseHelper db, int mes, String tipo ) {
        Double valor = new Double(0);
        String SQL = "SELECT SUM(valor) AS TOTAL FROM movimentacao WHERE "
                        + IDatabaseHelper.COL_TIPO + " =?"
                        + " AND strftime('%m',data) =?"
                        + " AND " + COL_ID_USUARIO + " =?";

        String where[] =  new String[]{tipo, mes < 10 ? "0" : "" + String.valueOf(mes), String.valueOf(usuarioLogado.getId_usuario())};

        this.db = db.getReadableDatabase();

        //Executar a operação de SELEÇÃO
        Cursor c = this.db.rawQuery(SQL,where);

        if (c.moveToFirst()){

            do{
                valor = c.getDouble( 0 );
            }while(c.moveToNext());

        }

        c.close();

        return valor;
    }

    public List<Movimentacao> pesquisarPorNome(DatabaseHelper db, String nome) {
        return null;
    }
}
