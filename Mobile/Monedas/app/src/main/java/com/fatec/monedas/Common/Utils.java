package com.fatec.monedas.Common;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

public class Utils {
    public static void showToast(String text, Context context) {
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public static String showDouble(double value){

        if(value == (long)value)
            return String.format("%d",(long)value);
        else
            return String.format("%s",value);
    }
}
