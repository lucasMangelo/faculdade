package com.fatec.monedas.Model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Movimentacao {
    private int id;
    private String descricao;
    private Double valor;
    private String tipo;
    private Date data;
    private String localizacao;
    private int id_usuario;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Movimentacao(int id, String descricao, Double valor, String tipo, String data){
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");

        try{
            this.id = id;
            this.descricao = descricao;
            this.valor = valor;
            this.tipo = tipo;
            this.data =  formato.parse(data);
        }
        catch(Exception ex){

        }
    }

    public Movimentacao(){}

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }
}
