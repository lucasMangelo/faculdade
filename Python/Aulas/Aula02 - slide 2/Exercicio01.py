print('Exercício 01')

produto = input('Informe o nome do produto ')
valor = float(input('Informe o valor do produto '))

porcentagem_desconto = 0

if valor <= 0:
    print('valor inválido')
    exit()
elif valor >= 50 and valor < 200:
    porcentagem_desconto = 5
elif valor >= 200 and valor < 500:
    porcentagem_desconto = 6
elif valor >= 500 and valor < 1000:
    porcentagem_desconto = 7
elif valor >= 1000:
    porcentagem_desconto = 8

desconto =  valor - ((valor * porcentagem_desconto) / 100)

print("desconto aplicado de: {0} . Valor atual: {1}".format(porcentagem_desconto, desconto))
