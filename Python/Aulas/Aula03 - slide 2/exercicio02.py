# Elabore uma aplicação receba um número
# indeterminado de valores informados pelo
# usuário.
# – Crie funções para determinar:
# • Quantidade de números pares
# • Quais são os números ímpares
# • O maior número
# • O menor número
# • A média dos números
# – Apresente os resultados na tela.

def DesejaContinuar():
    resposta = input('Deseja continuar? s (sim) ou n (nao) ').upper()
    if resposta == 'S':
        return True
    else:
        return False

def CapturaValor():
    try:
        return float(input("Informe um número: "))
    except ValueError:
        print('Valor inválido')
        return CapturaValor()

def RetornaQuantidadeNumeroPares(valores):
    quantidade = 0
    for valor in valores:
        if valor % 2 == 0:
            quantidade = quantidade + 1

    return quantidade

def RetornaQuantidadeNumeroImpares(valores):
    quantidade = 0
    for valor in valores:
        if valor % 2 != 0:
            quantidade = quantidade + 1

    return quantidade

def RetornaMedia(valores):
    return sum(valores) / float(len(valores))

def ExibeValores(valores):
    print("Quantidade de números pares: %s" %RetornaQuantidadeNumeroPares(valores))
    print("Quantidade de números impares: %s" %RetornaQuantidadeNumeroImpares(valores))
    print("Maior número: %s" %max(valores))
    print("Menor número: %s" %min(valores))
    print("Média: %s" %RetornaMedia(valores))

def Main():
    continua = True

    parametros = []

    while continua:
        parametros.append(CapturaValor()) 
        continua = DesejaContinuar()

    ExibeValores(parametros)

Main()