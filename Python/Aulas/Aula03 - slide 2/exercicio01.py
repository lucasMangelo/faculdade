def DesejaContinuar():
    resposta = input('Deseja continuar? s (sim) ou n (nao) ').upper()
    if resposta == 'S':
        return True
    else:
        return False

def CapturaValor(frase):

    exibir = "Informe {0}".format(frase)

    try:
        return float(input(exibir))
    except ValueError:
        print('Valor inválido')
        return CapturaValor(frase)

def CalculaIMC(peso, altura):
    return peso / pow(altura,2)
 

def Main():
    continua = True

    while continua:
        peso = CapturaValor('seu peso: ')
        altura = CapturaValor('sua altura: ')
        print('Seu IMC é: %f' %CalculaIMC(peso, altura))
        continua = DesejaContinuar()

Main()