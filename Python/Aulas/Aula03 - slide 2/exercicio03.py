# – Elabore uma aplicação capaz de gerar o currículo
# de uma pessoa em HTML.
# – Os parâmetros para o currículo são: nome,
# endereço, telefone, e-mail, escolaridade e
# experiência profissional.
# – Você pode utilizar as tags HTML da sua
# preferência.
# – Utilize funções para organizar o código fonte da
# aplicação.

def CapturaValor(frase):
    return input("Informe %s: " %frase)

def CurriculoHTML(nome, endereco, telefone, email, escolaridde, experienciaProfissional):
    html = """
    <html>
        <head>
            <meta charset="UTF-8">
            <title> Currículo de {0} </title>
        </head>
        <body style="text-align: center;font-size: 1.8em;">
            <h1> Currículo </h1>
            <br/>
            <h4> Nome: {0}</h4>
            <p> Endereco: {1}</p> 
            <p> Telefone: {2}</p>
            <p> Email: {3}</p>
            <p> Escolaridade: {4}</p>   
            <br/>
            <br/>
            <p> Experiencias Profissionais: {5}</p> 
        </body>
    </html>
    """.format(nome, endereco, telefone, email, escolaridde, experienciaProfissional)

    return html

def RetornaNomeDoArquivo(nome):
    return 'curriculo_{0}.html'.format(nome)

def GeraArquivo(conteudo, nomeArquivo):
    file = open(nomeArquivo, 'w', encoding='utf-8')
    file.write(conteudo)
    file.close()

def Main():
    nome = CapturaValor('seu nome')
    endereco = CapturaValor('seu endereço')
    telefone = CapturaValor('seu telefone')
    email = CapturaValor('seu e-mail')
    escolaride = CapturaValor('sua escolaridade')
    experienciaProfissional = CapturaValor('sua experiência profissional')

    curriculo = CurriculoHTML(nome, endereco, telefone, email, escolaride, experienciaProfissional)

    nomeArquivo = RetornaNomeDoArquivo(nome)

    GeraArquivo(curriculo,nomeArquivo)

    print("Currículo gerado com sucesso")

    print("Por favor, acesse o arquivo %s" %nomeArquivo)


Main()