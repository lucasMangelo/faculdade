import random as rnd

def createAray(pos, max):
    return [ rnd.randint(1, max) for _ in range(pos)]

def adjacentElementsProduct(array):
    elements = []
    for x in range(len(array)):
        adj =  array[x + 1] if len(array) - 1 != x else 0
        elements.append(adj * array[x])
    return max(elements)  

array = createAray(5, 10)

print(array)

print(adjacentElementsProduct(array))


