def checkPalindrome(word):
    aux = list(word)
    aux.reverse()
    return ''.join(aux) == word

word =  input('Informe uma palavra: ')

isPalindrome = checkPalindrome(word)

if isPalindrome == True:
    print('É uam palavra palíndroma')
else:
    print('Não é uma palavra palíndroma')