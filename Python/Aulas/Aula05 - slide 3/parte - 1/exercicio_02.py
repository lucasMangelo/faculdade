import random as rnd

def createAray(pos, max):
    return [ rnd.randint(1, max) for _ in range(pos)]

array = createAray(10, 100)
maxValue = max(array)

print(array)

print("maior valor {0}".format(maxValue))

array_calculed = list(map(lambda x: x / maxValue,array))

print(array_calculed)