import collections
import string
import secrets

User = collections.namedtuple("User", "Name UserName Password")

def WishContinue():
    resposta = input('Deseja continuar? s (sim) ou n (nao) ').upper()
    if resposta == 'S':
        return True
    else:
        return False

def getPassword():
    alphabet = string.ascii_letters + string.digits
    return ''.join(secrets.choice(alphabet) for i in range(8))

def getUserName(users, name):
    words = list(name.split(' '))
    userName = words[0][0] + ''.join(words[1:])

    names = list(filter(lambda x: x.UserName == userName, users))
    
    if(len(names) > 0):
        userName += str(len(names))

    return userName

def AddUsuer(users):
    name = input("Informe o nome completo do usuário: ")
    userName = getUserName(users, name)
    password = getPassword()
    return User(name, userName, password)

def ShowUsers(users):
    print("Nome         Usuario         Senha")        
    for user in users:
        print("{0}         {1}         {2}".format(user.Name,user.UserName,user.Password))

def Main():
    users = []
    continua = True

    while continua:
        users.append(AddUsuer(users)) 
        continua = WishContinue()

    ShowUsers(users)

Main()