# Exercício 1

print('Cálculo de área de triângulos')

altura = float(input('Informe a altura do triângulo: '))
base = float(input('Informe a base do triângulo: '))

area = (altura * base) / 2

print('A área do triângulo é: %.2f' %area)