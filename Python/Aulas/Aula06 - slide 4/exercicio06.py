import json
import requests

urlGetMarcas = 'http://fipeapi.appspot.com/api/1/carros/marcas.json'
urlGetCarroPorMarca = 'http://fipeapi.appspot.com/api/1/carros/veiculos/{0}.json'
urlGetCarroPorId = 'http://fipeapi.appspot.com/api/1/carros/veiculo/{0}/{1}.json'

marcaSelecionada = ''
carroSelecionado = ''

marcas = requests.get(url=urlGetMarcas).json()

if len(marcas) == 0:
    print('nenhuma marca encontrada')
    exit()

nomesMarcas = list(map(lambda x: x['fipe_name'], marcas))

print('Marcas:\n {0}'.format(nomesMarcas))

nomeMarca = input('Qual marca deseja filtrar?: ')

marcaSelecionada = list(filter(lambda x: x['fipe_name'] == nomeMarca, marcas))

if len(marcaSelecionada) == 0:
    print('Marca {0} não encontrada'.format(marcaSelecionada))

carros = requests.get(url=urlGetCarroPorMarca.format(marcaSelecionada[0]['id'])).json()

if len(carros) == 0:
    print('nenhuma carro encontrado')
    exit()

nomesCarros = list(map(lambda x: x['fipe_name'], carros))

print('Carros da marca {0}:\n'.format(nomesCarros))

nomeCarro = input('Qual carro deseja filtrar?: ')

carroSelecionado = list(filter(lambda x: x['fipe_name'] == nomeCarro, carros))

if len(carroSelecionado) == 0:
    print('Marca {0} não encontrada'.format(nomeCarro))

carro = requests.get(url=urlGetCarroPorMarca.format(marcaSelecionada[0]['id'], carroSelecionado[0]['id'])).json()

if len(carro) == 0:
    print('carro selecionado não foi encontrado')
    exit()

print(carro)