import csv
import json

nome = 'data.csv'

nomeJson = nome.split('.')[0] + ".json"

arquivo = open(nome,'r', encoding='utf-8')
arquivoJson = open(nomeJson,'w')

reader = csv.reader(arquivo)

data = []

for row in reader:
    jsonObj = {}

    for cell in row:
        jsonObj[row.index(cell)] = cell

    data.append(jsonObj)

json.dump(data, arquivoJson, sort_keys=True, indent=4)

arquivo.close()
arquivoJson.close()