def FilterEvenNumbers(vetor):
    return list(filter(lambda x: x % 2 == 0,vetor))

def FilterOddNumbers(vetor):
    return list(filter(lambda x: x % 2 != 0,vetor))

def FilterPositiveNumbers(vetor):
    return list(filter(lambda x: x >= 0,vetor))

vetor = []
    
for i in range(6):
    vetor.append(int(input('Informe o {0}º número: '.format(i + 1))))

length = len(vetor)
evenNumbers = FilterEvenNumbers(vetor)
oddNumbers = FilterOddNumbers(vetor)

print('Quantidade de números pares: {0}'.format(len(evenNumbers)))
print('Números ímpares: {0}'.format(oddNumbers))
print('Soma de todos os números: {0}'.format(sum(vetor)))
print('Maior número: {0}'.format(max(vetor)))
print('Menor número: {0}'.format(min(vetor)))
print('Quantidade de números positivos: {0}'.format(len(FilterPositiveNumbers(vetor))))