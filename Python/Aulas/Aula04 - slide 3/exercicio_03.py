import collections
import os

os.system("cls")

Aluno = collections.namedtuple("Aluno", "Nome Prova_1 Prova_2 Media Situacao")

alunos = []

for i in range(6):
    print("Informações do aluno nº {0}".format(i + 1))
    nome = input('Digite o nome: ')
    prova_1 = int(input('Digite a nota da 1ª prova: '))
    prova_2 = int(input('Digite a nota da 2ª prova: '))
    media = (prova_1 + prova_2) / 2
    if media >= 6:
        situacao = "Aprovado"  
    elif media >= 5 and media < 6:
        situacao = "Recuperação"
    else:
        situacao = "Reprovado"
    alunos.append(Aluno(nome, prova_1, prova_2, media, situacao))

print('\n\n')

print("Aluno    1ª Prova    2ª Prova    Média   Situação")

for aluno in alunos:
    print("{0}    {1}    {2}    {3}   {4}".format(aluno.Nome, aluno.Prova_1, aluno.Prova_2, aluno.Media, aluno.Situacao))

medias = list(map(lambda x: x.Media,alunos)) 
aprovados = list(filter(lambda x: x.Situacao == "Aprovado" ,alunos))
recuparacao = list(filter(lambda x: x.Situacao == "Recuperação" ,alunos))
reprovados = list(filter(lambda x: x.Situacao == "Reprovado" ,alunos))

print("Média da classe: {0}".format(sum(medias)/len(medias)))
print("Quantidade de aprovados: {0}".format(len(aprovados)))
print("Quantidade de alunos de exames: {0}".format(len(recuparacao)))
print("Quantidade de reprovados: {0}".format(len(recuparacao)))