import collections
import os

os.system("cls")

Produto = collections.namedtuple("Produto", "Nome Preco")

produtos = []

for i in range(5):
    print("Informações do produto nº {0}".format(i + 1))
    nome = input('Digite o nome: ')
    preco = float(input('Digite o preço: '))   
    produtos.append(Produto(nome, preco))

print('\n\n')

precos = list(map(lambda x: x.Preco,produtos)) 
inferiores_50 = list(filter(lambda x: x.Preco < 50 ,produtos))
entre_50_100 = list(map(lambda x: x.Nome,filter(lambda x: x.Preco >= 50 and x.Preco <= 100 ,produtos)))
superiores_100 = list(filter(lambda x: x > 100 ,precos))
media_superiores_100 = sum(superiores_100)/len(superiores_100) if len(superiores_100) > 0 else 0


print("Quantidade de produtos com preço infeiror a R$ 50,00: {0}".format(len(inferiores_50)))
print("Nome dos produtos com preço entre R$ 50,00 e R$ 100,00: {0}".format(entre_50_100))
print("Média de produtos superiores a R$ 100,00: {0}".format(media_superiores_100))