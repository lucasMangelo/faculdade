import os

def showOptions():
    print('     Pet-Shop')
    print('[1] Adicionar Pet')
    print('[2] Relatório de Pet')
    print('[3] Relatório de Pet por Espécie')
    print('[4] Exportar dados')
    print('[0] Sair')

def getOption():
    return input('Opção: ')

def doOption(option):

    try:
        option = int(option)
    except:
        print('É preciso informar um número')

    if option == 0:
        exit() 
    else:
        print('Opção inválida')

def main():
    showOptions()
    option = getOption()
    os.system('cls')
    doOption(option)

main()

