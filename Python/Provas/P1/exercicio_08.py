import collections
import os

Animal = collections.namedtuple('Animal', 'id nome especie raca data_de_nascimento sexo')

animals = []

def showOptions():
    print('     Pet-Shop')
    print('[1] Adicionar Pet')
    print('[2] Relatório de Pet')
    print('[3] Relatório de Pet por Espécie')
    print('[4] Exportar dados')
    print('[0] Sair')

def getOption():
    return input('Opção: ')

def doOption(option):

    try:
        option = int(option)
    except:
        print('É preciso informar um número')

    if option == 1:
        addAnimal()
    elif option == 2:
        showReport(animals)
    elif option == 3:
        showReportBySpecie()
    elif option == 0:
        exit() 
    else:
        print('Opção inválida')

def addAnimal():
    name = input('Informe o nome do animal: ')
    specie = input('Informe a especie do animal: ')
    race = input('Informe a raca do animal: ')
    birthDate = input('Informe a data de nascimento do animal: ')
    genre = input('Informe o sexo do animal: ')

    id = len(animals) + 1 

    animals.append(Animal(id, name, specie, race, birthDate, genre))

def showReport(data):
    print('ID       Nome        Especie         Raca        Data de Nascimento      Sexo')
    for animal in data:
        print('{0}       {1}        {2}         {3}        {4}      {5}'.format(animal.id, animal.nome, animal.especie, animal.raca, animal.data_de_nascimento, animal.sexo))

    print('\n\n')
        
def showReportBySpecie():
    os.system('cls')
    specie = input('Informe a especie que deseja filtrar')

    data =list(filter(lambda x: x.especie == specie, animals))

    if len(data) == 0:
        print('Não foram encontrados animais desta espécie cadastrados')
    else:
        showReport(data)

def main():
    while True:
        showOptions()
        option = getOption()
        os.system('cls')
        doOption(option)

main()