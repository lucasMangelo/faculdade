## Clonar repositorio ##

User eses passos para clonar este repositório. A clonagem permite o trabalho com arquivos locais.

1. Acesse o Overview do projeto csci-app
2. Copie o link HTTP ou SSH nesta página.
3. Abra o prompt de comando em um diretório local, digite o comando $ git clone  + link copiado.
4. O repositório foi clonado com sucesso.

Para mais dúvidas acesse este [Como clonar um repositório](https://confluence.atlassian.com/x/4whODQ)

## Adicionar Arquivos ##

Adicionar por arquivo:

1. Abra o prompt de comando e digite $ git add nome_do_arquivo

Adicionar todos os arquivos:

1. Abra o prompt de comando e digite $ git add -A

## Commit ##

Após adicionar o arquivo é necessário realizar o commit.

1. Abra o prompt de comando e digite $ git add -a -m "comentário do commit"

## Enviar mudanças para o repositório ##

Após o commit é necessário enviar os arquivos alterados através do comando push

1. Abra o prompt de comando e digite $ git push origin branche_atual

## Atualizar diretório local ##

1. Abra o prompt de comando e digite $ git pull origin branche_atual

## Criar ou trocar de branch ##

Para trocar de branch é necessário utilizar o comando checkout. Exemplo:

1. Abra o prompt de comando e digite $ git checkout nome_branch

Para criar uma nova branch é necessário adicionar o parâmetro -b.

1. Abra o prompt de comando e digite $ git checkout -b nome_branch