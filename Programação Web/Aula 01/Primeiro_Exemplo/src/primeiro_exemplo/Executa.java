/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primeiro_exemplo;

/**
 *
 * @author lu-ta
 */
public class Executa {
    public static void main(String[] args){
        Conta c1 = new Conta();
        System.out.println("Saldo conta corrente: " + c1.getSaldo());
        c1.deposito(100);
        System.out.println("Saldo conta corrente após depositar: " + c1.getSaldo());
        
        
        ContaCorrente c2 = new ContaCorrente();
        System.out.println("Saldo conta corrente: " + c2.getSaldo());
        c2.limiteChEsp(500);
        System.out.println("Saldo conta corrente após aumentar limite do cheque especial: " + c2.getSaldo());
        
        c2.deposito(250);
        System.out.println("Saldo conta corrente após depositar: " + c2.getSaldo());
        
        c2.saque(300);
        System.out.println("Saldo conta corrente após sacar um valor maior que o saldo: " + c2.getSaldo());
        
        ContaPoupanca c3 = new ContaPoupanca();
        
        c3.deposito(250);
        System.out.println("Saldo conta poupanca após depositar: " + c3.getSaldo());
        
        c3.setRendimento(0.5);
        System.out.println("Calcula rendimento de 5% a.m em 5 meses na conta poupanca : " + c3.calculaRend(5));
    }
}
