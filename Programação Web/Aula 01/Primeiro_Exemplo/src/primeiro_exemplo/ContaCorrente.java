/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primeiro_exemplo;

/**
 *
 * @author lu-ta
 */
public class ContaCorrente extends Conta{
    
    private double cheque_especial;
    
    public void limiteChEsp(double vr){
        cheque_especial += vr;
    }
    
    @Override
    public double getSaldo(){
        return super.getSaldo() + this.cheque_especial;
    }
    
    @Override
    public double saque(double vr){
        
        if(saldo > vr)
            saldo -= vr;
        else if (saldo < vr){
            saldo -= vr;
            cheque_especial += saldo;
            saldo = 0;
        }
        
        return saldo + this.cheque_especial;
    }
}
