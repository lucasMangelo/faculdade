/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primeiro_exemplo;

/**
 *
 * @author lu-ta
 */
public class Conta
{
	
    protected int num;
    protected String nome;
    protected double saldo;

    public double saque(double vr)
    {
        if(saldo > vr)
                saldo -= vr;
        return saldo;
    }

    public void deposito(double vr){
        saldo += vr;
    }

    public double getSaldo(){
        return saldo;
    }
    
    
}