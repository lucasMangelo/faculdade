/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primeiro_exemplo;
import java.math.*;

/**
 *
 * @author lu-ta
 */
public class ContaPoupanca extends Conta{
    
    private double rendimento;
    
    public void setRendimento(double vr){
        this.rendimento = vr;
    }
    
    public double calculaRend(int meses)
    {
        if(this.rendimento == 0)
            return this.saldo;
        
        return this.saldo * Math.pow((1 + (this.rendimento / 100)),meses);
    }
}
