/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Controller.ContatosJpaController;
import Models.Contatos;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author lu-ta
 */
@ManagedBean
@RequestScoped
public class BeanContatos {

    public Contatos getContato() {
        return contato;
    }

    public void setContato(Contatos contato) {
        this.contato = contato;
    }
    
    private Contatos contato;
    private ContatosJpaController contatoDAO;
    
    /**
     * Creates a new instance of BeanContatos
     */
    public BeanContatos() {
        contato = new Contatos();
        contatoDAO = new ContatosJpaController(javax.persistence.Persistence.createEntityManagerFactory("AgendaWebPU"));
    }
    
    public void Inserir(){
        try{
            contatoDAO.create(contato);
        }
        catch(Exception ex){
            
        }
    }
    
    public void Atualizar(){
        try{
            contatoDAO.edit(contato);
        }
        catch(Exception ex){
            
        }
    }
    
    
    public void Excluir(){
        try{
            contatoDAO.destroy(contato.getId());
        }
        catch(Exception ex){
        }
    }
}
