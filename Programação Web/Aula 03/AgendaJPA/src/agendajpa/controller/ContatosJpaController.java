/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendajpa.controller;

import agendajpa.controller.exceptions.NonexistentEntityException;
import agendajpa.entities.Contatos;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author lu-ta
 */
public class ContatosJpaController implements Serializable {

    public ContatosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public Contatos create(Contatos contatos) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(contatos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        
        return contatos;
    }

    public void edit(Contatos contatos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            contatos = em.merge(contatos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = contatos.getId();
                if (findContatos(id) == null) {
                    throw new NonexistentEntityException("The contatos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Contatos contatos;
            try {
                contatos = em.getReference(Contatos.class, id);
                contatos.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The contatos with id " + id + " no longer exists.", enfe);
            }
            em.remove(contatos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Contatos> findContatosEntities() {
        return findContatosEntities(true, -1, -1);
    }

    public List<Contatos> findContatosEntities(int maxResults, int firstResult) {
        return findContatosEntities(false, maxResults, firstResult);
    }

    private List<Contatos> findContatosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Contatos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Contatos findContatos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Contatos.class, id);
        } finally {
            em.close();
        }
    }
    
    public List<Contatos> findByName(Contatos contatos){
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Contatos> query = em.createNamedQuery("Contatos.findByNome",Contatos.class);
            query.setParameter("nome", contatos.getNome());
            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public int getContatosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Contatos> rt = cq.from(Contatos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
