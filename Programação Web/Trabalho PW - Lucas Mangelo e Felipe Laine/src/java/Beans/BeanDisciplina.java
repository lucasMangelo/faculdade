/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Controller.CursoJpaController;
import Controller.DisciplinaJpaController;
import Model.Curso;
import Model.Disciplina;
import java.lang.annotation.Annotation;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.persistence.Converter;

/**
 *
 * @author Felipe
 */
@ManagedBean
@RequestScoped
public class BeanDisciplina {
    private Disciplina disciplina;
    private DisciplinaJpaController disciplinadao;
    
    /**
     * Creates a new instance of BeanDisciplina
     */
    public BeanDisciplina() {
        disciplina = new Disciplina();
        disciplinadao = new DisciplinaJpaController(javax.persistence.Persistence.createEntityManagerFactory("AvaliacaoPU"));
    }
    
    
    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }
    
        public void inserir(){
        try{
            disciplinadao.create(disciplina);
        }catch(Exception ex){
            
        }
    }
    
    public void atualizar(){
        try{
            disciplinadao.edit(disciplina);
        }catch(Exception ex){
            
        }
    }
    
    public void excluir(String id){
        try{
            disciplinadao.destroy(Integer.parseInt(id));
        }catch(Exception ex){
            
        }
    }
    
    public List<Disciplina> listar(){
        return disciplinadao.findDisciplinaEntities();
    }
    
}

