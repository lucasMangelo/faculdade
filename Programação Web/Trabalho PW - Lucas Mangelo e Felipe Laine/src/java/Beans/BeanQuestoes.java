/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Controller.QuestoesJpaController;
import Model.Questoes;
import Model.Questoes;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Felipe
 */
@ManagedBean
@RequestScoped
public class BeanQuestoes {
    private Questoes questoes;
    private QuestoesJpaController questoesdao;
    /**
     * Creates a new instance of BeanQuestao
     */
    public BeanQuestoes() {
        questoes = new Questoes();
        questoesdao = new QuestoesJpaController(javax.persistence.Persistence.createEntityManagerFactory("AvaliacaoPU"));
    }
       public Questoes getQuestoes() {
        return questoes;
    }

    public void setQuestoes(Questoes questoes) {
        this.questoes = questoes;
    }
    
        public void inserir(){
        try{
            questoesdao.create(questoes);
        }catch(Exception ex){
            
        }
    }
    
    public void atualizar(){
        try{
            questoesdao.edit(questoes);
        }catch(Exception ex){
            
        }
    }
    
    public void excluir(String id){
        try{
            questoesdao.destroy(Integer.parseInt(id));
        }catch(Exception ex){
            
        }
    }
    
    public List<Questoes> listar(){
        return questoesdao.findQuestoesEntities();
    }
}
