/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import Controller.CursoJpaController;
import Model.Curso;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Felipe
 */
@ManagedBean
@RequestScoped
public class BeanCurso {
    
    private Curso curso;
    private CursoJpaController cursodao;
    /**
     * Creates a new instance of BeanCurso
     */
    public BeanCurso() {
        curso = new Curso();
        cursodao = new CursoJpaController(javax.persistence.Persistence.createEntityManagerFactory("AvaliacaoPU"));
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
    
        public void inserir(){
        try{
            cursodao.create(curso);
        }catch(Exception ex){
            
        }
    }
    
    public void atualizar(){
        try{
            cursodao.edit(curso);
        }catch(Exception ex){
            
        }
    }
    
    public void excluir(String id){
        try{
            cursodao.destroy(Integer.parseInt(id));
        }catch(Exception ex){
            
        }
    }
    
    public List<Curso> listar(){
        return cursodao.findCursoEntities();
    }
}
