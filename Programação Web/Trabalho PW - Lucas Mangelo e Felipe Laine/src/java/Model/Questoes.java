/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Felipe
 */
@Entity
@Table(name = "questoes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Questoes.findAll", query = "SELECT q FROM Questoes q")
    , @NamedQuery(name = "Questoes.findById", query = "SELECT q FROM Questoes q WHERE q.id = :id")
    , @NamedQuery(name = "Questoes.findByIdDisciplina", query = "SELECT q FROM Questoes q WHERE q.idDisciplina = :idDisciplina")
    , @NamedQuery(name = "Questoes.findByDescricao", query = "SELECT q FROM Questoes q WHERE q.descricao = :descricao")
    , @NamedQuery(name = "Questoes.findByTipoAvaliacao", query = "SELECT q FROM Questoes q WHERE q.tipoAvaliacao = :tipoAvaliacao")})
public class Questoes implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_disciplina")
    private int idDisciplina;
    @Basic(optional = false)
    @Column(name = "descricao")
    private String descricao;
    @Basic(optional = false)
    @Column(name = "tipo_avaliacao")
    private String tipoAvaliacao;

    public Questoes() {
    }

    public Questoes(Integer id) {
        this.id = id;
    }

    public Questoes(Integer id, int idDisciplina, String descricao, String tipoAvaliacao) {
        this.id = id;
        this.idDisciplina = idDisciplina;
        this.descricao = descricao;
        this.tipoAvaliacao = tipoAvaliacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getIdDisciplina() {
        return idDisciplina;
    }

    public void setIdDisciplina(int idDisciplina) {
        this.idDisciplina = idDisciplina;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipoAvaliacao() {
        return tipoAvaliacao;
    }

    public void setTipoAvaliacao(String tipoAvaliacao) {
        this.tipoAvaliacao = tipoAvaliacao;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Questoes)) {
            return false;
        }
        Questoes other = (Questoes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Model.Questoes[ id=" + id + " ]";
    }
    
}
