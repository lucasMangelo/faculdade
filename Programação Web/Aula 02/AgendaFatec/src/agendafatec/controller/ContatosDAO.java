/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendafatec.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import agendafatec.model.Contatos;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 *
 * @author lu-ta
 */
public class ContatosDAO {
    
    //Gerenciar a conexão com BD
    private Connection con;
    //Envia as acoes para o BD
    private PreparedStatement cmd;
    
    public int inserir(Contatos c){
        try{
            con = Conexao.conectar();
            
            String sql = "INSERT INTO contatos(nome,email,telefone)"
                    + "values(?,?,?)";
            
            cmd = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            
            cmd.setString(1, c.getNome());
            cmd.setString(2, c.getEmail());
            cmd.setString(3, c.getTelefone());
            
            if(cmd.executeUpdate() > 0){
                ResultSet rs = cmd.getGeneratedKeys();
                return rs.next() ? rs.getInt(1) : -1;
            }
            else{
                
                return -1;
            }
        }
        catch(Exception ex){
            System.out.println("Erro:" + ex.getMessage());
            return -1;
        }
        finally{
            Conexao.desconectar(con);
        }
    }
    
    public boolean atualizar(Contatos c){
        try{
            con = Conexao.conectar();
            
            String sql = "UPDATE contatos SET nome=?,email=?,telefone=? WHERE id =?";
            
            cmd = con.prepareStatement(sql);           
            cmd.setString(1, c.getNome());
            cmd.setString(2, c.getEmail());
            cmd.setString(3, c.getTelefone());
            cmd.setInt(4, c.getId());
            
            return cmd.executeUpdate() > 0;
        }
        catch(Exception ex){
            System.out.println("Erro:" + ex.getMessage());
            return false;
        }
        finally{
            Conexao.desconectar(con);
        }
    }
    
    public boolean remover(Contatos c){
        try{
            con = Conexao.conectar();
            
            String sql = "DELETE contatos WHERE id =?";
            
            cmd = con.prepareStatement(sql);
            cmd.setInt(1, c.getId());
            
            return cmd.executeUpdate() > 0;
        }
        catch(Exception ex){
            System.out.println("Erro:" + ex.getMessage());
            return false;
        }
        finally{
            Conexao.desconectar(con);
        }
    }
    
    public List<Contatos> listar(){
        try{
            List<Contatos> lista = new List<Contatos>();
            String sql = "SELECT * FROM contatos";
            
            con = Conexao.conectar();
            cmd = con.prepareStatement(sql);
            ResultSet rs = cmd.executeQuery();
            
            while(rs.next()){
                Contatos c = new Contatos();
                c.setId(rs.getInt("id"));
                c.setNome(rs.getString("nome"));
                c.setEmail(rs.getString("email"));
                c.setEmail(rs.getString("telefone"));
                lista.add(c);
            }
            
            return lista;
        }
        catch(Exception ex){
            System.out.println("Erro:" + ex.getMessage());
            return null;
        }
        finally{
            Conexao.desconectar(con);
        }
    }
    
    public List<Contatos> pesquisar(Contatos contato){
        try{
            List<Contatos> lista = new List<Contatos>(); 
            String sql = "SELECT * FROM contatos WHERE nome LIKE ?";
            
            con = Conexao.conectar();
            cmd = con.prepareStatement(sql);
            cmd.setString(1,"%" + contato.getNome().toString() + "%");
            ResultSet rs = cmd.executeQuery();
            
            while(rs.next()){
                Contatos c = new Contatos();
                c.setId(rs.getInt("id"));
                c.setNome(rs.getString("nome"));
                c.setEmail(rs.getString("email"));
                c.setEmail(rs.getString("telefone"));
                lista.add(c);
            }
            
            return lista;
        }
        catch(Exception ex){
            System.out.println("Erro:" + ex.getMessage());
            return null;
        }
        finally{
            Conexao.desconectar(con);
        }
    }
}
