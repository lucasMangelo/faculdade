/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agendafatec.controller;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author lu-ta
 */
public class Conexao {
    private static final String DB = "agenda";
    private static final String DRIVER = "org.postgresql.Driver";
    private static final String SERVER = "localhost:5432";
    private static final String URL = "jdbc:postgresql://" + SERVER + "/"+DB;
    private static final String USR = "postgres";
    private static final String PWD = "1912";
    
    //método estático para a conexão
    public static Connection conectar(){
        try{
            Class.forName(DRIVER);
            return DriverManager.getConnection(URL,USR,PWD);
        }
        catch(Exception ex){
            System.out.println("Erro:" + ex.getMessage());
            return null;
        }
    }
    
    public static void desconectar(Connection conexao){
        try{
            conexao.close();
        }
        catch(Exception ex){
            System.out.println("Erro:" + ex.getMessage());
        }
    }
    
    public static void main(String args[]){
        Connection conexao = conectar();
        if(conexao != null){
            System.out.println("Conexão realizada com sucesso!");
            desconectar(conexao);
        }
    }
}
