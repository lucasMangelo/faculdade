import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sbn
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, confusion_matrix

df_traindata = pd.read_csv('train.csv')
df_testdata = pd.read_csv('test.csv')
df_testsubmission = pd.read_csv('gender_submission.csv')

x = df_traindata.drop('Name', axis=1).drop('Cabin', axis=1).drop('Embarked', axis=1).drop('Ticket', axis=1).drop('Sex', axis=1)
y = df_testdata.drop('Name', axis=1).drop('Cabin', axis=1).drop('Embarked', axis=1).drop('Ticket', axis=1).drop('Sex', axis=1)

clf = DecisionTreeClassifier(criterion='entropy')

x_train = x.drop('Survived', axis=1).values
y_train = x['Survived'].values

x_test = y.values
y_test = df_testsubmission['Survived'].values

clf.fit(x_train, y_train)

y_pred = clf.predict(x_test)

print(accuracy_score(y_true=y_test,y_pred=y_pred))