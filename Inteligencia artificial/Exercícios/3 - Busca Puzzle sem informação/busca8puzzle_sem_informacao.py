import time
from colorama import Fore
from random import shuffle, randrange
import matplotlib.pyplot as plt
import seaborn as sbn
import numpy as np

dim = (3,3)
free_space_symbol = 0
final_state = [[1,2,3],[4,5,6],[7,8,free_space_symbol]]

# support functions
def find_free_space(state):
    return [(idx, row.index(free_space_symbol)) for idx, row in enumerate(state) if free_space_symbol in row][0]

def copy_state(state):
    return [row[:] for row in state]

# actions
def up(state):
    r,c = find_free_space(state)
    new_state = copy_state(state)
    if r > 0:
        new_state[r][c], new_state[r-1][c] = new_state[r-1][c], new_state[r][c]
        return new_state
    return None

def down(state):
    r,c = find_free_space(state)
    new_state = copy_state(state)
    if r < dim[0]-1:
        new_state[r][c], new_state[r+1][c] = new_state[r+1][c], new_state[r][c]
        return new_state
    return None

def left(state):
    r,c = find_free_space(state)
    new_state = copy_state(state)
    if c > 0:
        new_state[r][c], new_state[r][c-1] = new_state[r][c-1], new_state[r][c]
        return new_state
    return None

def right(state):
    r,c = find_free_space(state)
    new_state = copy_state(state)
    if c < dim[1]-1:
        new_state[r][c], new_state[r][c+1] = new_state[r][c+1], new_state[r][c]
        return new_state
    return None

puzzle_problem = {
    'initial_state': None, 
    'goal_state': final_state, 
    'actions': {
        'Up': up, 
        'Down': down, 
        'Left': left, 
        'Right': right 
    } 
}

def create_initial_state(problem,moves=50):
    state = problem['goal_state']
    for _ in range(moves):
        successors = get_successors(problem, state)
        state = successors[randrange(0,len(successors))][1]
    return state

# strategies
# breadth and depth search remove
def remove_front(lst):
    return lst.pop(0)

# breadth search insert
def insert_all_breadth(lst,lste):
    lst.extend(lste)
    return lst

# depth search insert
def insert_all_depth(lste,lst):
    lste.extend(lst)
    return lste

# general algorithm

def create_node(state=None):
    return { 'parent': None, 'action': None, 'state': state, 'path_cost': 0, 'depth': 0 }

def goal_test(problem,state):
    return problem['goal_state'] == state

def step_cost(node,problem,s):
    return 1

def get_successors(problem, state):
    return [(name, action(state)) for name, action in problem['actions'].items() if action(state) != None]
    
def expand(node,problem):
    successors = []
    for action, result in get_successors(problem,node['state']):
        if result == None:
            continue
        s = create_node(result)
        s['parent'] = node
        s['action'] = action
        s['path_cost'] = s['path_cost'] + step_cost(node,problem,s)
        s['depth'] = node['depth'] + 1
        successors.append(s)
    return successors

def tree_search(
    problem,
    fringe,
    wait=2,
    verbose=True,
    strategy={'insert': insert_all_breadth, 'remove': remove_front},
    visit_control=True,
    recording_step=0,
    limit_depth=0):
    recording = {'iteration': [], 'depth': [], 'left_nodes': []}
    fringe.append(create_node(problem['initial_state']))
    visited = []
    i = 0
    while True:
        i += 1
        if fringe == []:
            return 'Failure', recording
        node = strategy['remove'](fringe)
        if recording_step > 0 and i % recording_step == 0:
            recording['iteration'].append(i)
            recording['depth'].append(node['depth'])
            recording['left_nodes'].append(len(fringe))
        if visit_control and node['state'] in visited:
            print(Fore.RED + 'Iteração:',i, 'Avaliando:',
                  node['state'], 'Profundidade:', node['depth'], 'Nós restantes:', len(fringe), '- Já avaliado')
            continue 
        visited.append(node['state'])
        print(Fore.BLUE + 'Iteração:', i, 'Avaliando:',
              node['state'], 'Profundidade:', node['depth'], 'Nós restantes:', len(fringe))
        if goal_test(problem,node['state']):
            return node, recording
        if limit_depth == 0 or (limit_depth > 0 and node['depth'] < limit_depth):
            fringe = strategy['insert'](expand(node,problem),fringe)
        if verbose:
            print(Fore.WHITE + "Fringe:",[n['state'] for n in fringe])
        time.sleep(wait)

#plotting facilities

def plot_recording(rec):
    plt.figure(figsize=(10, 5))
    
    plt.subplot(121)
    plt.plot(rec['depth'])
    plt.xlabel('Iteration')
    plt.ylabel('Depth')

    plt.subplot(122)
    plt.plot(rec['left_nodes'])
    plt.xlabel('Iteration')
    plt.ylabel('Number of Nodes')
    plt.tight_layout()

def show_solution(node_solution):
    state_seq = []
    node = node_solution
    while node != None:
        state_seq.append(node['state'])
        node = node['parent']
    state_seq = state_seq[::-1]
    col = len(state_seq) // 3 + len(state_seq) % 3
    _, axs = plt.subplots(3, col,figsize=(20,10))
    axs = np.reshape(axs,col*3)
    for i,state in enumerate(state_seq):
        sbn.heatmap(state,annot=True,linewidth = 0.5, ax=axs[i],cbar=False)
    for ax in axs:
        ax.set_axis_off()

initial = create_initial_state(puzzle_problem, 2)
initial_node = create_node(initial)
sucessors = expand(initial_node, puzzle_problem)
solution, record = tree_search(puzzle_problem, sucessors)
show_solution(solution)
plot_recording(record)
