import time
import random as rnd
from colorama import Fore
import datetime
import os

# global variables
dirty = 'D'
clean = ' '
width = 0
height = 0

statistics = {'step': 0, 'dirty_places': 0, 'start': None, 'end': None}

def inclement_step():
    statistics['step'] = statistics['step'] + 1

clear = lambda: os.system('cls')

def create_world(w=2,h=1,alpha=0.5):
    return [[clean if rnd.random() > alpha else dirty for _ in range(w)] for _ in range(h)]

# the fuckin' world
world = create_world(w=10, h=10, alpha=0.4)

# agent representation
vaccum_location = {'x': 0, 'y': 0}
vaccum = {'action': 'NoOp'}

# agent actions
def goSuck():
    world[vaccum_location['y']][vaccum_location['x']] = clean

def goRight():
    x = len(world[0])
    inclement_step()
    vaccum_location['x'] = vaccum_location['x'] + 1 if vaccum_location['x'] < x - 1 else vaccum_location['x']

def goLeft():
    inclement_step()
    vaccum_location['x'] = vaccum_location['x'] - 1 if vaccum_location['x'] > 0 else vaccum_location['x']

def goUp():
    inclement_step()
    vaccum_location['y'] = vaccum_location['y'] - 1 if vaccum_location['y'] > 0 else vaccum_location['y']

def goDown():
    y = len(world)
    inclement_step()
    vaccum_location['y'] = vaccum_location['y'] + 1 if vaccum_location['y'] < y - 1 else vaccum_location['y']

def NoOp():
    pass

vaccum_actions = {'Suck': goSuck, 'NoOp': NoOp, 'Right': goRight, 'Left': goLeft, 'Up': goUp, 'Down': goDown}

def agent_function(perception):
    location = perception['location']
    closer = perception['closer']
    status = perception['status']

    if status == dirty:
        return 'Suck'
    else:
        if location['x'] < closer['x']:
            return 'Right'
        elif location['x'] > closer['x']:
            return 'Left'
        elif location['y'] < closer['y']:
            return 'Down'
        elif location['y'] > closer['y']:
            return 'Up'
        return 'NoOp'
        
def get_dirty_places():
    dirty_places = []
    for y,row in enumerate(world):
        for x,col in enumerate(row):
            if(col == dirty):
                dirty_places.append({'x': x, 'y': y})
    return dirty_places

def get_closer_dirty_place():
    dirty_places = get_dirty_places()
    x = vaccum_location['x']
    y = vaccum_location['y']
    total = len(world) + len(world[0])

    if len(dirty_places) > 0:
        
        data = {'index':-1, 'distance': total}
        for x,row in enumerate(dirty_places):
            distance = abs(x - row['x']) + y - row['y']
            if distance < data['distance']:
                data = {'index':x, 'distance': distance}
        
        return dirty_places[data['index']]
    else:
        return {'x': x, 'y': y}
    

def percept():
    return {'location': vaccum_location, 
    'status': world[vaccum_location['y']][vaccum_location['x']],
    'closer': get_closer_dirty_place()}

def act(action):
    vaccum['action'] = action
    vaccum_actions[action]()

def show_word():
    print("That's the world ... \nVaccum: x: {0} y: {1} Action: {2}\n".format(vaccum_location['x'],vaccum_location['y'], vaccum['action']))
    for y,row in enumerate(world):
        for x,col in enumerate(row):
            agent = '.' if x ==  vaccum_location['x'] and y == vaccum_location['y'] else ''
            place = '' if agent == '.' and col != dirty else col
            print('[{0}{1}]'.format(place,agent),end=' ')
        print('\n')

def set_start_process():
    statistics['start'] = datetime.datetime.now()

def set_end_process():
    statistics['end'] = datetime.datetime.now()

def show_statistics():
    time = "minutes: {0} seconds: {1}".format(abs(statistics['end'].minute - statistics['start'].minute), abs(statistics['end'].second - statistics['start'].second))  

    print('\nSteps: {0}, dirty places: {1}, time: {2}'.format(statistics['step'],statistics['dirty_places'], time))

def evolves_environment():
    keep = True
    statistics['dirty_places'] = len(get_dirty_places())
    set_start_process()
    while keep:
        clear()
        show_word()
        perception = percept()
        action = agent_function(perception)
        act(action)
        time.sleep(0.3)
        keep = True if action != 'NoOp' else False

    set_end_process()

    show_statistics()

evolves_environment()